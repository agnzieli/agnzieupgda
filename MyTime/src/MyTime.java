
public class MyTime {

	int hour = 0;
	int minute = 0;
	int second = 0;

	public MyTime() {

	}

	public MyTime(int hour, int minute, int second) {
		setTime(hour, minute, second);
	}

	public void setTime(int hour, int minute, int second) {
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		if (hour >= 0 && hour < 24) {
			this.hour = hour;
		}
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		if (minute >= 0 && minute <= 59) {
			this.minute = minute;
		}
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		if (second >= 0 && second <= 59) {
			this.second = second;
		}
	}

	private String leadZero(int num) {
		if (num < 10) {
			return "0" + num;
		} else {
			return "" + num;
		}
	}

	public String toString() {
		return leadZero(this.hour) + ":" + leadZero(this.minute) + ":" + leadZero(this.second);
	}

	public MyTime nextSecond() {
		int newSecond = second + 1;
		if (newSecond == 60) {
			newSecond = 0;
			int newMinute = minute + 1;
			if (newMinute == 60) {
				newMinute = 0;
				int newHour = hour + 1;
				if (newHour == 24){
					newHour = 0;
				}
				return new MyTime(newHour, newMinute, newSecond);
			}

		}

		return new MyTime(hour, minute, newSecond);

		// if (getSecond() == 59){
		// second = 0;
		// minute += minute;
		// }
		// else {
		// second = second + 1;
		// }
		//
		// return new MyTime(hour, minute, second);
	}

	public MyTime nextMinute() {
		int newMinute = minute + 1;
		if (newMinute == 60) {
			newMinute = 0;
			int newHour = hour + 1;
			if (newHour == 24){
				newHour = 0;
			}
			return new MyTime(newHour, newMinute, second);
		}

		return new MyTime(hour, newMinute, second);

	}

	public MyTime nextHour() {

		int newHour = hour + 1;
		if (newHour == 24) {
			newHour = 0;
		}

		return new MyTime(newHour, minute, second);

	}

	public MyTime previousHour() {

		int newHour = hour - 1;
		if (newHour == -1) {
			newHour = 23;
		}

		return new MyTime(newHour, minute, second);

	}
	
	public MyTime previousMinute() {
		int newMinute = minute - 1;
		if (newMinute == -1) {
			newMinute = 59;
			int newHour = hour - 1;
			if (newHour == -1) {
				newHour = 23;
				return new MyTime(newHour, newMinute, second);
			}
			
		}

		return new MyTime(hour, newMinute, second);

	}
	
	public MyTime previousSecond() {
		int newSecond = second - 1;
		if (newSecond == -1) {
			newSecond = 59;
			int newMinute = minute - 1;
			if (newMinute == -1) {
				newMinute = 59;
				int newHour = hour - 1;
				if (newHour == -1) {
					newHour = 23;
					return new MyTime(newHour, newMinute, newSecond);
				}
				
			}
		}

		return new MyTime(hour, minute, newSecond);
	}

}
