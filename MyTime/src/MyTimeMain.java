
public class MyTimeMain {

	public static void main(String[] args) {
		MyTime mt = new MyTime(13, 59, 2);
		System.out.println("" + mt.nextMinute());
		MyTime mt1 = new MyTime(00, 00, 00);
		System.out.println("" + mt1.previousSecond());
		MyTime mt2 = new MyTime(23, 59, 59);
		System.out.println("" + mt2.nextSecond());
		MyTime mt3 = new MyTime(12, 20, 39);
		System.out.println("" + mt3.nextSecond());

	}

}
