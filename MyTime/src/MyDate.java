
public class MyDate {
	
	private int year;
	private int month;
	private int day;
	
	private static String[] strMonths = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	
	private static String[] strDays = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
	
	private  static int[] daysInMonths = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	
	public static boolean isLeapYear (int year){
		if (year % 4 == 0 && year % 100 != 0 && year % 400 == 0){
		return true;
		}
		else
			return false;
	}
	
	public static boolean isValidDate(int year, int month, int day){
		return false;
	}
	
	public static int getDayOfWeek(int year, int month, int day){
		int y = year % 1000;
		double w = day + (2.6*month - 0.2) + y + y/4   ;  
		return 0;
	}
	
	public MyDate(int year, int month, int day){
		setDate(year, month, day);
	}

	public void setDate(int year, int month, int day) {
		this.year = year;
		this.month = month;
		this.day = day;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month>0 && month < 13){
		this.month = month;
		}
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day > 0 && day < 32){
		this.day = day;
		}
	}
	
	
	
	
	
	
	
}
