package pl.org.pfig.calc;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SimpleCalcDivideMethodTest {
	
	private SimpleCalc sc;
	
	@Before
	public void doStuff(){
		sc = new SimpleCalc();
	}	

	@Test
	public void whenTwoPositiveNumbersAreGivenPositiveNumberIsQuotient(){
		int a = 8.0, b = 2.0;
		assertEquals(4.0, sc.divide(a, b));
	}
	
	@Test
	public void whenTwoNegativeNumbersAreGivenPosotiveNumberIsQuotient(){
		int a = -8, b = -2;
		assertEquals(4.0, sc.divide(a, b));
	}
	
	@Test
	public void whenDividendIsPositiveNumbersAndDivisorIsNegativeNegativeNumberIsQuotient(){
		int a = 8, b = -2;
		assertEquals(-4.0, sc.divide(a, b));
	}
	
	@Test
	public void whenDividendIsNegativeNumbersAndDivisorIsPositiveNegativeNumberIsQuotient(){
		int a = -8, b = 2;
		assertEquals(-4.0, sc.divide(a, b));
	}
	
//	@Test(expected=ArithmeticException.class)
//	public void whenDivisorIsZeroExceptionIsThrown(){
//		int a = 8, b = 0;
//		assertEquals(sc.divide(a, b));
//		
//	}
	

}
