package pl.org.pfig.calc;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SimpleCalcAddMethodTest {
	
	private SimpleCalc sc;
	
	@Before
	public void doStuff(){
		sc = new SimpleCalc();
	}
	

	@Test
	public void whenTwoPositiveNumbersAreGivenPositiveNumberAreSum() {
		int a = 3, b = 6;
		assertEquals(9, sc.add(a, b));
		
	}
	
	@Test(expected=Exception.class)
	public void whenExThrowMethodIsUsedExceptionIsThrown() throws Exception{
		sc.exThrow();
	}
	
	@Test
	public void whenTwoNegativeNumbersAreGivenNegativeNumberIsSum(){
		int a = -2, b = -8;
		assertEquals(-10, sc.add(a, b));
	}
	
	@Test
	public void whenNegativeAndPositiveNumberAreGivenNegativeOrPositiveNumberIsSum(){
		int a = -2, b = 8;
		assertEquals(6, sc.add(a, b));
	}
	
	@Test
	public void whenPositiveAndNegaiveNumberAreGivenNegativeOrPositiveNumberIsSum(){
		int a = 2, b = -8;
		assertEquals(-6, sc.add(a, b));
	}
	
	@Test
	public void whenTwoMaxIntegersAreGivenPositiveNumberAreExpected(){
		assertTrue("Out of range", sc.add(Integer.MAX_VALUE, Integer.MAX_VALUE) > 0);
	}

}
