package pl.org.pfig.calc;

public class SimpleCalc {
	
	public int add(int a, int b){
		return a+b;
	}
	
	public int exThrow() throws Exception{
		throw new Exception("Wyjatek");
	}
	
	public double divide(double a, double b) {
		
		return a / b;
		
	}

}
