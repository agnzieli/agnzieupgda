package students;

import java.util.LinkedList;
import java.util.List;

public class FileTest {

	public static void main(String[] args) {

		List<Student> students = new LinkedList<>();

		students.add(new Student(100, "jan", "kowalski"));
		students.add(new Student(101, "jan1", "kowalski1"));
		students.add(new Student(102, "jan2", "kowalski2"));

		University u = new University(students);

		TextFile textFile = new TextFile();

		textFile.save(u.getStudentsList());

		List<Student> studentsFromFile = textFile.load();
		University universityFromFile = new University(studentsFromFile);

		universityFromFile.showAll();
	}

}
