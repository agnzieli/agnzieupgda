package students;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class University {
	private Map<Integer, Student> students = new HashMap<>();

	public University() {}

	public University(List<Student> students) {
		for (Student student : students) {
			this.students.put(student.getIndexNumber(), student);
		}
	}

	public void addStudent(int indexNumber, String name, String surname) {
		Student student = new Student(indexNumber, name, surname);
		students.put(student.getIndexNumber(), student);
	}

	public boolean studentExists(int indexNumber) {
		// Delegacja
		return students.containsKey(indexNumber);
	}

	public Student getStudent(int indexNumber) {
		// Delegacja
		return students.get(indexNumber);
	}

	public int studentsNumber() {
		return students.size();
	}

	public void showAll() {
		for (Student s : students.values()) {
			System.out.println(s.getIndexNumber() + " : " + s.getName() + " " + s.getSurname());
		}
	}

	public List<Student> getStudentsList() {
		
		List<Student> sList = new LinkedList<>();

        for (Student s : students.values()) {

            sList.add(s);

        }

        return sList;
	}

	public static void main(String[] args) {
		
		List<Student> students = new LinkedList<>();
		
		students.add(new Student(100, "jan", "kowalski"));
		students.add(new Student(101, "jan1", "kowalski1"));
		students.add(new Student(102, "jan2", "kowalski2"));
		
		University u = new University(students);
		u.showAll();
		
//		for (Map.Entry<Integer, Student> entry : u.students.entrySet()){
//			System.out.println(entry);
//		}
//		
		for (Student s : u.getStudentsList() ){
			System.out.println(s);
		}
	}
}
