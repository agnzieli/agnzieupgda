package pl.sdacademy.moviedb;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sdacademy.crud.SimpleCRUD;
import pl.sdacademy.entity.Genre;
import pl.sdacademy.entity.Movie;
import pl.sdacademy.util.HibernateUtil;

import java.util.Scanner;

/**
 * Created by RENT on 2017-07-13.
 */
public class MovieDB {
    public static void main(String[] args) {

        SimpleCRUD<Movie> cm = new SimpleCRUD<Movie>(Movie.class);

        Movie movie1 = new Movie();
        Genre genre1 = new Genre();

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj tytuł filmu: ");
        String title = sc.nextLine();
        movie1.setTitle(title);
        System.out.println("Podaj rok wydania filmu: ");
        int year = Integer.parseInt(sc.nextLine());
        movie1.setYear(year);
        System.out.println("Podaj czas trwania filmu: ");
        double dur = Double.parseDouble(sc.nextLine());
        movie1.setDuration(dur);
        System.out.println("Podaj opis filmu: ");
        String desc = sc.nextLine();
        movie1.setDescription(desc);
        System.out.println("Nowy gatunek: ");
        String name = sc.nextLine();
        genre1.setName(name);
        movie1.setGenre(genre1);
        sc.close();

        Movie movie2 = new Movie("Pulp fiction", 1989, 240, "Super film", new Genre("komedia"));

        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.save(genre1);
        session.save(movie1);
        session.save(movie2);
        t.commit();
        session.close();
    }
}
