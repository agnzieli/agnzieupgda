package pl.sdacademy.entity;

import javax.persistence.*;

/**
 * Created by RENT on 2017-07-13.
 */

@Entity
@Table(name = "movie")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "year")
    private int year;
    @Column(name = "duration")
    private double duration;
    @Column (name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    private Genre genre;

    public Movie() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Movie(String title, int year, double duration, String description) {
        this.title = title;
        this.year = year;
        this.duration = duration;
        this.description = description;
    }

    public Movie(String title, int year, double duration, String description, Genre genre) {
        this.title = title;
        this.year = year;
        this.duration = duration;
        this.description = description;
        this.genre = genre;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
}


