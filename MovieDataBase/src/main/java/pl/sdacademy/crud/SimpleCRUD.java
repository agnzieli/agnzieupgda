package pl.sdacademy.crud;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sdacademy.util.HibernateUtil;

/**
 * Created by RENT on 2017-07-13.
 */
public class SimpleCRUD<T> implements CRUD<T> {
    public Class<T> clazz;
    public SimpleCRUD(Class<T> clazz){
        this.clazz = clazz;
    }

    public void insertOrUpdate(Object obj) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        int newId = Integer.valueOf(session.save(obj) + "");
        System.out.println("Umieszczono rekord o ID = " + newId);
        session.saveOrUpdate(obj);
        t.commit();
        session.close();
    }

    public void delete(Object obj) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(obj);
        t.commit();
        session.close();
    }

    public T select(int id) {
        T object;
        Session session = HibernateUtil.openSession();
        object = (T)session.load(clazz, id);
        session.close();
        return object;
    }

//    public void List<T> select() {
//        List<T> list;
//        Session session = HibernateUtil.openSession();
//        list = session.createQuery("from ").list();
//        session.close();
//
//        return list;
//
//    }
}
