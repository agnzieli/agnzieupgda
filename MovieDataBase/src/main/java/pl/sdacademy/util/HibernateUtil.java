package pl.sdacademy.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 * Created by RENT on 2017-07-13.
 */
public class HibernateUtil {

    //private static HibernateUtil hu;
    private static SessionFactory sessionFactory = null;

//    public static HibernateUtil getInstance(){
//        if (hu == null){
//            hu = new HibernateUtil();
//        }
//        return hu;
//    }

    private static SessionFactory buildSessionFactory(){
        if (sessionFactory == null){
            sessionFactory = new Configuration().configure().buildSessionFactory();
        }
        return sessionFactory;
    }

    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }

    public static Session openSession(){
        return getSessionFactory().openSession();
    }

}
