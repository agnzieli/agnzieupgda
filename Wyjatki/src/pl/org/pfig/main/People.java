package pl.org.pfig.main;

import java.util.LinkedList;

public class People {
	
	private LinkedList<Person> ppl = new LinkedList<>();
	private final String[] allowedEyes = {"green", "brown", "blue", "black"};
	private final String[] allowedHair = {"blond", "brown", "black", "red"};
	
	int max = 0;
	
	public People(int max){
		this.max = max - 1;
	}
	
	private boolean isFull() throws FullListException {
		if(ppl.size() >= max){
		throw new FullListException();
		}
		return false;
	}
	
	public void addPerson(Person person) throws FullListException{
		if(!isFull()){
			ppl.add(person);
		}
	}
	
	public void addPerson (String name, String secondName, int age, String hair, String eyes, double shoe) 
							throws BadHairException, BadEyesException, BadShoeException{
		if(!inArray(hair, allowedHair)){
			throw new BadHairException();
		}
		if(!inArray(eyes, allowedEyes)){
			throw new BadEyesException();
		}
		if(shoe% 0.5 != 0) {
			throw new BadShoeException();
		}
//		if(!isFull()){
//		ppl.add(new Person());
//		} 
	}
	
	private boolean inArray(String needle, String[] haystack){
		for(String s : haystack){
			if(s.equals(needle)){
				return true;
			}
		}
		
		return false;
	}
	
	
	
	

}
