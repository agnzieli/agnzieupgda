package pl.org.pfig.main;

public class Square {
	
	public static double square (int n) throws SquareException{
		if (n<0) {
			throw new SquareException ("n jest mniejsze od 0!");
		} else
		return Math.sqrt(n);
	}

}
