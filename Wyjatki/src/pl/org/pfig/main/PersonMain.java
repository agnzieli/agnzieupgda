package pl.org.pfig.main;

public class PersonMain {

	public static void main(String[] args) {
		
//		Person p = new Person();
//		
//		try {
//			p.setAge(-2);
//		} catch (WrongAgeException e) {
//			System.out.println(e.getMessage());
//			
//		}
		
		People people = new People(2);
		try {
			people.addPerson(new Person("a", "b", 1, "c", "d", 12.5));
			people.addPerson(new Person("aa", "bb", 11, "cc", "dd", 22.5));
			people.addPerson(new Person("aaa", "bbb", 111, "ccc", "ddd", 1112.5));
		} catch (FullListException e){
			System.out.println(e.getMessage());
		}
		
		

	}

}
