package pl.org.pfig.main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
	//	ExceptionSimpleClass esc = new ExceptionSimpleClass();

//		try {
//			esc.exceptionExample("pawel");
//		} catch (PawelException e) {
//			System.out.println(e.getMessage());
//		}
		
		ReadNumbers rn = new ReadNumbers();
		System.out.println(rn.readDouble());
		System.out.println(rn.readInt());
		System.out.println(rn.readString());

		try {
			System.out.println(Square.square(-9));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} 
		
		try {
			System.out.println(Division.divide(9, 0));
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			System.out.println(Division.divide(2.676, 4.9876));
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
		
		QuadraticEquation qe = new QuadraticEquation();
		
		double[] res = qe.solve();
		
		for(int i = 0; i < res.length; i++){
			System.out.println("x" + (i+1) + " = " + res[i]);
		}
		
		//
		// try {
		// esc.make(55);
		// } catch (IllegalArgumentException e){
		// e.printStackTrace();
		// //System.out.println("[ERROR] " + e.getMessage());
		// }
		// catch(Exception e){
		// System.out.println(e.getMessage());
		// } finally {
		// System.out.println("[END]");
		// }
//
//		int[] array = new int[10];
//		Scanner scanner = new Scanner(System.in);
//
//		for (int i = 0; i < 10; i++) {
//			System.out.println("Element o indeksie [ " + i + " ]");
//			int b = scanner.nextInt();
//			array[i] = b;
//		}
//		try {
//			System.out.println(array[10]);
//
//		} catch (ArrayIndexOutOfBoundsException e) {
//			System.out.println("Brak elementu pod indeksem " + e.getMessage());
//
//		}

	}
}
