package pl.org.pfig.main;

public class Division {
	
	public static double divide (int a, int b) throws IllegalArgumentException {
		if (b == 0){
			throw new IllegalArgumentException("Dzielna nie mo�e by� r�wna 0!");
		} else {
			return a/b;
		}
		
	}
	public static double divide (double a, double b) throws IllegalArgumentException{
		if (b == 0){
			throw new IllegalArgumentException("Dzielna nie mo�e by� r�wna 0!");
		} else {
			return a/b;
		}
		
	}

}
