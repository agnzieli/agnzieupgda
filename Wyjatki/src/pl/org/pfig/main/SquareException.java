package pl.org.pfig.main;

public class SquareException extends Exception {
	
	public SquareException(String message){
		super(message);
	}

}
