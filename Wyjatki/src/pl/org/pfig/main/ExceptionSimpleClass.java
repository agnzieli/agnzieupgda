package pl.org.pfig.main;

public class ExceptionSimpleClass {

	public void make(int a) throws IllegalArgumentException, Exception {
		
		if (a == 5) {
			throw new Exception("a jest r�wne 5");
		} 
		
		if (a == 55) {
			throw new IllegalArgumentException("Niepoprawny argument");
		}
	}
	
	public void exceptionExample(String name) throws PawelException{
		if(name.equalsIgnoreCase("pawel")) {
			throw new PawelException("Jestem pawe�em");
		}
		
	}

}
