
public class RepoName {

	private String name;
	private String secondName;
	
	public RepoName(String name, String secondName) {
		this.name = name;
		this.secondName = secondName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public static String getRepoName(String name, String secondName) {
		int cut = 3;
		if (name.toLowerCase().charAt(2) == secondName.toLowerCase().charAt(0)) {
			++cut;
		}
			return name.toUpperCase().charAt(0) 
					+ name.toLowerCase().substring(1, cut) 
					+ secondName.toUpperCase().charAt(0) 
					+ secondName.toLowerCase().substring(1, cut) 
					+ "UPgda";				
		}
		

	public String getRepoName() {
		return getRepoName(name, secondName);
	}

	}



