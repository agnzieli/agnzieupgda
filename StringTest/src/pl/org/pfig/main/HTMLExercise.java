package pl.org.pfig.main;

public class HTMLExercise {
	private String str = "";
	
	public HTMLExercise(String str)
	{
		this.str = str;
	}
	public HTMLExercise strong(){
		this.str = "<strong>" + this.str + "</strong>";
		return this;
	}
	
	public HTMLExercise p(){
		this.str = "<p>" + this.str + "/p>";
		return this;
	}
	
	public HTMLExercise print(){
		System.out.println(this.str);
		return this;
	}
}
