package pl.org.pfig.main;

import java.util.Random;

public class StringUtil {
	private String str;
	
	public StringUtil(String str){
		this.str = str;
	}
	
	public StringUtil print(){
		System.out.println(str);
		return this;
	}
	
	public StringUtil prepend(String arg){
		str = arg + str;
		return this;
	}
	
	public StringUtil append(String arg){
		str = str + arg;
		return this;
	}
	
	public StringUtil letterSpacing(){
		String[] arr = str.split("");
		str = "";
		for(String s : arr){
			str += s + " ";
		}
		str = str.substring(0, str.length()-1);
		return this;	
	}
	
	public StringUtil reverse(){
		String[] arr = str.split("");
		str = "";
			for(int j = arr.length-1; j >= 0; j--){
				str = str + arr[j];
			}
			
			return this;
			
			
		}
		
	
	public StringUtil getAlphabet(){
		String alfabet ="";
		for(char c = 97; c <= 122; c++){
			alfabet += c;
		}
		str = str.replaceAll(str, alfabet);
		
		return this;
		
	}
	
	public StringUtil getFirstLetter(){
		String[] arr = str.split("");
		str = str.replaceAll(str, arr[0]);
		return this;
		
	}
	
	public StringUtil limit(int n){
		if(n<str.length()){
		str = str.substring(0, n);
		}
		return this;
	}
	
	public StringUtil insertAt(String s, int n){
		if(n<str.length()){
		str = str.substring(0, n).concat(s).concat(str.substring(n, str.length()));
		}
		return this;
	}
	
	public StringUtil resetText(){
		str = str.replaceAll(str, "");
		return this;
	}
	
	public StringUtil swapLetters(){
		String firstLetter = "" + str.charAt(0);
		String lastLetter = "" + str.charAt(str.length()-1);
		str = lastLetter + str.substring(1, str.length()-1) + firstLetter;
		return this;
		
	}
	
	public StringUtil createSentence(){
		String firstLetter = ("" + str.charAt(0)).toUpperCase();
		String[] arr = str.split("");
		if (arr[arr.length-1] != "." ){
		str = firstLetter + str.substring(1, str.length()) + ".";
		}
		else{
			str =  firstLetter + str.substring(1, str.length());
		}
		return this;
		
	}
	
	public StringUtil cut (int from, int to){
		String[] arr = str.split("");
		str = "";
		for (int i = 0; i< from; i++){
			str += arr[i];			
		}
		for (int i= to; i < arr.length-1; i++){
			str += arr[i];
		}
		return this;
	}
	
	public StringUtil pokemon(){
		String newString = "";
		for (int i = 0; i < str.length(); i++){
			if ( i % 2 == 0){
				newString += (str.charAt(i) + "").toLowerCase();
			}
			else {
				newString += (str.charAt(i) + "").toUpperCase();
			}
		}
		return this;
		
	}
	
	public StringUtil getRandomHash(int n) {
		Random r = new Random();
		String ret = "";
		for (int i=0; i<n; i++){
		if(r.nextInt(2) == 1) {
			ret += (char) r.nextInt(6) + 97;
		}
		else{
			ret += (char) r.nextInt(10) + 48;
			
		}
		}
		str = ret;
		return this;
	}
	
	
	
	
}

