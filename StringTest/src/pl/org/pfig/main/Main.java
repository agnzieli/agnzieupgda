package pl.org.pfig.main;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		String myStr = "  przykladowy ciag znakow  ";
		
		if (myStr.equals("  przykladowy ciag znakow  ")){
			System.out.println("Ci�gi s� takie same.");
		}
		
		if(myStr.equalsIgnoreCase("  PRZYKLADOWY ciag znaKOW  ")) {
			System.out.println("Ci�gi znak�w s� takie same bez badania wielko�ci liter.");
		}
		
		System.out.println("dlugosc myStr to: " + myStr.length());
		
		System.out.println(myStr.substring(14));
		
		String otherStr = "PAWELEK";
		
		System.out.println(otherStr.substring(1));

		System.out.println(otherStr.substring(1, 4));
		
		System.out.println(otherStr.substring(1, otherStr.length()-1));
		
		System.out.println(myStr.trim());
		
		System.out.println(myStr.charAt(2) + "" + myStr.charAt(3));
		
		String alfabet ="";
		for(char c = 97; c <= 122; c++){
			alfabet += c;
		}
		
		System.out.println(alfabet);
		
		System.out.println( myStr.replaceAll("ciag", "lancuch"));
		
		System.out.println(myStr.concat(otherStr));
		
		System.out.println(myStr);
		
		if(myStr.contains("klad")){
			System.out.println("Slowo klad zawiera sie w " + myStr);
		}
		
		if(alfabet.startsWith("a")){
			System.out.println("alfabet zaczyna sie od A");
		}
		
		if(alfabet.endsWith("z")){
			System.out.println("Alfabet konczy sie na Z");
		}
		
		System.out.println(myStr.indexOf("a"));
		System.out.println(myStr.lastIndexOf("a"));
		
		String simpleStr = "pawel poszedl do lasu";
		String[] arrOfStr = simpleStr.split(" "); //rozbicie stringa na tablice przez spacje
		for(String s : arrOfStr){
			System.out.println("\t" + s);
		}
		
		String s = "oksymoron";
		String firstLetter = "" + s.charAt(0);
		System.out.println(firstLetter + s.substring(1).replace(firstLetter, "_"));
		
		Random r = new Random();
		//jezeli nie przekazjemy parametru, moze zostac zworocona liczba jemna
		System.out.println(r.nextInt());
		//jezeli przekazujemy arg, zostanie zwrocona liczba z zakresu <0, liczba)
		System.out.println(r.nextInt(15));
		
		
//		public static String[] checkWordsLength(int n, String s){
//			String[] words = s.split(" ");
//			String[] ret = new String[words.length];
//			int i = 0;
//			for(String word : words){
//				if(word.length()>n){
//					ret[i++]=word; // najpierw uzywamy liczby, a potem ja zwiekszamy po u�yciu
//			}
//							
//				
//			}
//			
//			
//			return ret;
//	
			
			
		}
		



	}


