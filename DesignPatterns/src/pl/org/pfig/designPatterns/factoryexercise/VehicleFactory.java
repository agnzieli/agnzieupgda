package pl.org.pfig.designPatterns.factoryexercise;

/**
 * Created by RENT on 2017-06-22.
 */
public class VehicleFactory {
    public static VehicleInterface getVehicle(String vehicle){
        vehicle = vehicle.toLowerCase();
        switch (vehicle){
            case "car":
                return new Car();
            case "tir":
                return new Tir();
            case "bus":
                return new Bus();
        }
        throw new IllegalArgumentException();
    }
}
