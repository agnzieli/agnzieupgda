package pl.org.pfig.designPatterns.factoryexercise;

/**
 * Created by RENT on 2017-06-22.
 */
public class Car implements VehicleInterface {
    @Override
    public void getVehicle() {
        System.out.println("Samochód jedzie");
    }
}
