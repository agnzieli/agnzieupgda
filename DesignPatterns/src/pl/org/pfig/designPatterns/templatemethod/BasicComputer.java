package pl.org.pfig.designPatterns.templatemethod;

/**
 * Created by RENT on 2017-06-22.
 */
public abstract class BasicComputer {

    public void devices(){
        motherboard();
        processor();
        externalDevice();
        System.out.println();
    }

    public void motherboard(){
        System.out.println("Motherboard");
    }

    public void processor(){
        System.out.println("Processor");
    }

    public abstract  void externalDevice();


}
