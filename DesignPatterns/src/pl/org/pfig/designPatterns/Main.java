package pl.org.pfig.designPatterns;

import pl.org.pfig.designPatterns.factory.AnimalFactory;
import pl.org.pfig.designPatterns.factory.AnimalInterface;
import pl.org.pfig.designPatterns.factoryexercise.VehicleFactory;
import pl.org.pfig.designPatterns.factoryexercise.VehicleInterface;
import pl.org.pfig.designPatterns.factoryother.Coupon;
import pl.org.pfig.designPatterns.factoryother.CouponFactory;
import pl.org.pfig.designPatterns.factoryother.People;
import pl.org.pfig.designPatterns.fluidinterface.Beer;
import pl.org.pfig.designPatterns.fluidinterface.Person;
import pl.org.pfig.designPatterns.singleton.ClassHost;
import pl.org.pfig.designPatterns.singleton.SingletonExample;
import pl.org.pfig.designPatterns.templatemethod.Laptop;
import pl.org.pfig.designPatterns.templatemethod.MidiTower;
import pl.org.pfig.designPatterns.templatemethod.PersonalComputer;
import pl.org.pfig.designPatterns.templatemethoduniversities.Biotechnology;
import pl.org.pfig.designPatterns.templatemethoduniversities.Electronics;
import pl.org.pfig.designPatterns.templatemethoduniversities.Informatics;

import javax.sound.sampled.Line;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        /* Singleton Pattern */
        SingletonExample se1 = SingletonExample.getInstance();
        SingletonExample se2 = SingletonExample.getInstance();
        se1.setName("agnieszka");
        se2.getName();

        ClassHost ch1 = ClassHost.getHost();
        ClassHost ch2 = ClassHost.getHost();

        /* Template Method Pattern */
        Laptop laptop = new Laptop();
        System.out.println("Laptop: " );
        laptop.devices();

        MidiTower mt = new MidiTower();
        System.out.println("MidiTowerComputer: ");
        mt.devices();

        PersonalComputer pc = new PersonalComputer();
        System.out.println("Personal Computer: ");
        pc.devices();

        Informatics inf = new Informatics();
        System.out.println("Informatics subjects: ");
        inf.university();

        Electronics ele = new Electronics();
        System.out.println("Electronics subjects: ");
        ele.university();

        Biotechnology bt = new Biotechnology();
        System.out.println("Biotechnology subjects: ");
        bt.university();

        /* Factory Pattern */
        AnimalInterface cat = AnimalFactory.getAnimal("cat");
        cat.getSound();

        AnimalInterface dog = AnimalFactory.getAnimal("dog");
        dog.getSound();

        AnimalInterface frog = AnimalFactory.getAnimal("frog");
        frog.getSound();

       // AnimalInterface tiger = AnimalFactory.getAnimal("tiger");

        /* Factory Pattern 2 */
        int[] numbers = new int[6];
        for(int i = 0; i < numbers.length; i++){
            numbers[i] = new Random().nextInt(48) + 1;
        }

        Coupon c = CouponFactory.getCoupon(numbers);

        System.out.println(c.getA());

        VehicleInterface car = VehicleFactory.getVehicle("car");
        car.getVehicle();

        Beer b = new Beer();
        b
                .setName("Johannes")
                .setTaste("bitter")
                .setType("pale")
                .setPrice(4.50);

        System.out.println(b);

        /*  FLUID INTERFACE*/

        List<Person> people = new LinkedList<>();

        people.add(new Person("Paweł", "Testowy", 30));
        people.add(new Person("Piotr", "Testowy", 42));
        people.add(new Person("Paweł", "Nowak", 24));
        people.add(new Person("Anna", "Kaczmarek", 22));

        People pp = new People();

        pp.addGroup("staff", people);

        for(Person pers : pp.from("staff").lastname("Testowy").name("Piotr").get()){
            System.out.println(pers);
        }

    }
}
