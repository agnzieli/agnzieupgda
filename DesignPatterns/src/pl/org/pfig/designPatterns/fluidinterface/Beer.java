package pl.org.pfig.designPatterns.fluidinterface;

/**
 * Created by RENT on 2017-06-22.
 */
public class Beer {
    private String name;
    private String taste;
    private String type;
    private double price;

    public Beer(){ }

    public Beer setName(String name) {
        this.name = name;
        return this;
    }

    public Beer setTaste(String taste) {
        this.taste = taste;
        return this;
    }

    public Beer setType(String type) {
        this.type = type;
        return this;
    }

    public Beer setPrice(double price) {
        this.price = price;
        return this;
    }

    public String getName() {
        return name;
    }

    public String getTaste() {
        return taste;
    }

    public String getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Beer{" +
                "name='" + name + '\'' +
                ", taste='" + taste + '\'' +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }
}
