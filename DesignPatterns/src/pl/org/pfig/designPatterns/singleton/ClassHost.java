package pl.org.pfig.designPatterns.singleton;

/**
 * Created by RENT on 2017-06-22.
 */
public class ClassHost {
    private static ClassHost _host;

    private ClassHost() {}

    public static ClassHost getHost () {
        //lazy initialization
        if (_host == null){
            System.out.println("Tworze gospodarza.");
            _host = new ClassHost();
        }
        System.out.println("Mamy już gospodarza. Idzie na zebranie");
        return _host;
    }
}
