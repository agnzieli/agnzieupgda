package pl.org.pfig.designPatterns.templatemethoduniversities;

/**
 * Created by RENT on 2017-06-22.
 */
public class Electronics extends University {
    @Override
    public void specialisticSubject() {
        System.out.println("Semi-conductors");
    }
}
