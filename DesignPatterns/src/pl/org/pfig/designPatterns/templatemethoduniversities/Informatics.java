package pl.org.pfig.designPatterns.templatemethoduniversities;

/**
 * Created by RENT on 2017-06-22.
 */
public class Informatics extends University {
    @Override
    public void specialisticSubject() {
        System.out.println("Algorithms");
    }
}
