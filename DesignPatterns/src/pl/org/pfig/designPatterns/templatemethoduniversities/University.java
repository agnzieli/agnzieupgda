package pl.org.pfig.designPatterns.templatemethoduniversities;

/**
 * Created by RENT on 2017-06-22.
 */
public abstract class University {

    public void university(){
        maths();
        physics();
        foreignLanguage();
        specialisticSubject();
        System.out.println();
    }

    public void maths(){
        System.out.println("Maths");
    }

    public void physics(){
        System.out.println("Physics");
    }

    public void foreignLanguage(){
        System.out.println("Foreign language");
    }

    public abstract void specialisticSubject();
}
