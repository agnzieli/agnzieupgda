package pl.org.pfig.designPatterns.factoryother;

public class CouponFactory {

    private CouponFactory() {
    }

    public static Coupon getCoupon(int[] numbers) {
        return new Coupon(numbers[0],
                numbers[1],
                numbers[2],
                numbers[3],
                numbers[4],
                numbers[5]);
    }
}
