package pl.org.pfig.designPatterns.decorator;

/**
 * Created by RENT on 2017-06-23.
 */
public class SelfieMaking implements Sportsman {

    private Sportsman sportsman;

    public SelfieMaking(Sportsman sportsman){
        this.sportsman = sportsman;
    }

    @Override
    public void prepare() {
        sportsman.prepare();
        System.out.println("Czas na selfie");
    }

    @Override
    public void doPumps(int number) {
        sportsman.doPumps(10);
        System.out.println("Czas na selfie");
    }

    @Override
    public void doSquats(int number) {
        sportsman.doSquats(20);
        System.out.println("Czas na selfie");
    }

    @Override
    public void doCrunches(int number) {
        sportsman.doCrunches(50);
        System.out.println("Czas na selfie");
    }

}
