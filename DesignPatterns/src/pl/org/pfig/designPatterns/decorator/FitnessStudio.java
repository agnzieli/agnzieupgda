package pl.org.pfig.designPatterns.decorator;

/**
 * Created by RENT on 2017-06-23.
 */
public class FitnessStudio {

    public void train(Sportsman sportsman){
        sportsman.prepare();
        sportsman.doPumps(5);
        sportsman.doSquats(8);
        sportsman.doCrunches(8);
    }
}
