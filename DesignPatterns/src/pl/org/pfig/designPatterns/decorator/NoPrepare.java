package pl.org.pfig.designPatterns.decorator;

/**
 * Created by RENT on 2017-06-23.
 */
public class NoPrepare implements Sportsman {

    private Sportsman sportsman;

    public  NoPrepare(Sportsman sportsman){
        this.sportsman = sportsman;
    }
    @Override
    public void prepare() {
        System.out.println("Nie robię dziś rozgrzewki");
    }

    @Override
    public void doPumps(int number) {
        sportsman.doPumps(number);
    }

    @Override
    public void doSquats(int number) {
        sportsman.doSquats(number);
    }

    @Override
    public void doCrunches(int number) {
        sportsman.doCrunches(number);
    }
}
