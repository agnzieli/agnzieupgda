package pl.org.pfig.designPatterns.decorator.Person;

/**
 * Created by RENT on 2017-06-23.
 */
public class PersonBuilder {

    private PersonPrinter personP = new BasicDataPrinter();

    public PersonBuilder printsAge(){
        personP = new AgePrinter(personP);
        return this;
    }

    public PersonBuilder printsWeight(){
        personP = new WeightPrinter(personP);
        return this;
    }

    public PersonBuilder printsHeight(){
        personP = new HeightPrinter(personP);
        return this;
    }

    public PersonBuilder printsEyesColor(){
        personP = new EyesColorPrinter(personP);
        return this;
    }

    public static PersonBuilder currentPerson(){
        return new PersonBuilder();
    }

    public PersonPrinter getP(){
        return personP;
    }


}
