package pl.org.pfig.designPatterns.decorator.Person;

import java.io.File;

import static pl.org.pfig.designPatterns.decorator.Person.PersonBuilder.currentPerson;


/**
 * Created by RENT on 2017-06-23.
 */
public class Main {

    public static void main(String[] args) {

       // PersonPrinter personPrinter = new AgePrinter(new EyesColorPrinter(new HeightPrinter(new BasicDataPrinter())));

        Person person = new Person("Jan", "Kowalski", 34, 85.5, 184.5, Person.EyesColor.BROWN);
       // personPrinter.print(person, System.out);


        /* FLUENT INTERFACE */

        PersonPrinter persPrint =   currentPerson()
                                    .printsAge()
                                    .printsEyesColor()
                                    .printsHeight()
                                    .getP();

        FilePrinter fp = new FilePrinter(persPrint);
        fp.printToFile(person);

    }
}
