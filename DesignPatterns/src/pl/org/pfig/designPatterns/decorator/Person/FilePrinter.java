package pl.org.pfig.designPatterns.decorator.Person;

import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class FilePrinter {

    private final PersonPrinter pp;

    public FilePrinter(PersonPrinter pp){
        this.pp = pp;
    }

    public void printToFile(Person person){
        try (PrintStream out = new PrintStream("out.txt"))
        {
            pp.print(person,out);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
