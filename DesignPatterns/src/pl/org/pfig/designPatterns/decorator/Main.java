package pl.org.pfig.designPatterns.decorator;

/**
 * Created by RENT on 2017-06-23.
 */
public class Main {

    public static void main(String[] args) {

//        Sportsman sportsman = new WaterDrinking(new BasicSportsman());
//        sportsman.prepare();

        FitnessStudio fs = new FitnessStudio();
        //fs.train(sportsman);

        Sportsman sp1 = new WaterDrinking(new DoubleSeries(new BasicSportsman()));
        fs.train(sp1);

    }
}