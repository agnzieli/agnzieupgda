package pl.org.pfig.designPatterns.decorator;


public interface Sportsman {
    public void prepare();
    public void doPumps(int number);
    public void doSquats(int number);
    public void doCrunches(int number);
}
