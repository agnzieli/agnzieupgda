package pl.org.pfig.designPatterns.decorator;

/**
 * Created by RENT on 2017-06-23.
 */
public class BasicSportsman implements Sportsman {
    @Override
    public void prepare() {
        System.out.println("Rozgrzewam się!");

    }

    @Override
    public void doPumps(int number) {
        for(int i = 1; i <= number; i++){
            System.out.println(i + " pompka ");
        }

    }

    @Override
    public void doSquats(int number) {
        for(int i = 1; i <= number; i++) {
            System.out.println(i + " przysiad ");
        }

    }

    @Override
    public void doCrunches(int number) {
        for(int i = 1; i <= number; i++){
            System.out.println(i + " brzuszek ");}

    }
}
