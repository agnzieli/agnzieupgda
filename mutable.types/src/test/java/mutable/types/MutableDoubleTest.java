package mutable.types;

import static org.junit.Assert.*;

import org.junit.Test;

public class MutableDoubleTest {

	@Test
	public void test() {
		
		MutableDouble a = new MutableDouble(5.67);
		MutableDouble b = a;
		
		a.setValue(8.77);
		
		assert a.getValue() == 8.77;
		assert b.getValue() == 8.77;
	}

}

