package mutable.types;

import static org.junit.Assert.*;

import org.junit.Test;

public class DoubleTest {

	@Test
	public void test() {
		
		Double a = 5.55;
		Double b = a;
		a = 8.99;
		
		assert a == 8.99;
		assert b == 5.55;


		
	}

}
