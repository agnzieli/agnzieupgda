package family.dziedziczenie;

public class Mother extends FamilyMember {
	
	private String name;
	public Mother (String name){
		super(name);
	}
	
	@Override
	public void introduce(){
		System.out.println("I'm a mother. My name is " + this.getName());
	}

}
