package family.dziedziczenie;

public class Doughter extends FamilyMember {

	private String name;
	public Doughter (String name){
		super(name);
	}
	
	@Override
	public void introduce(){
		System.out.println("I'm a doughter. My name is " + this.getName());
	}

}

