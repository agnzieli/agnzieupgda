package family.dziedziczenie;

public class Family {
	
	public static void main(String[] args) {
		FamilyMember mother = new Mother("Gra�yna");
		FamilyMember father = new Father("Janusz");
		FamilyMember son = new Son ("Adam");
		FamilyMember doughter = new Doughter("Anna");
		
		FamilyMember[] members = {mother, father, son, doughter};
		
		for(FamilyMember familyMember : members){
			familyMember.introduce();
		}
		
		mother.introduce();
		father.introduce();
		son.introduce();
		doughter.introduce();	
	}
	}
