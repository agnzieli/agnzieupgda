package family;

public class Family {
	
	public static void main(String[] args) {
		Mother mother = new Mother("Gra�yna");
		Father father = new Father("Janusz");
		Son son = new Son ("Adam");
		Doughter doughter = new Doughter("Anna");
		
		introduce(mother);
		introduce(father);
		introduce(son);
		introduce(doughter);
		
	}
	
	public static void introduce(Mother mother){
		System.out.println("I'm a mother. My name is " + mother.getName());
	}
	
	public static void introduce(Father father){
		System.out.println("I'm a father. My name is " + father.getName());
	}
	
	public static void introduce(Doughter doughter){
		System.out.println("I'm a doughter. My name is " + doughter.getName());
	}
	
	public static void introduce(Son son){
		System.out.println("I'm a son. My name is " + son.getName());
	}
	

}
