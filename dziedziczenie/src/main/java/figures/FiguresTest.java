package figures;

public class FiguresTest {
	
	public static void main(String[] args) {
			
	Square square = new Square(2.0);
	Circle circle = new Circle(1.0);
	
	System.out.println("Pole kwadratu: " + square.countArea());
	System.out.println("Obw�d kwadratu: " + square.countCircumference());
	System.out.println("Pole ko�a: " + circle.countArea());
	System.out.println("Obw�d kwadratu: " + circle.countCircumference());
	
	Figure[] figures = {new Square(2.0), new Circle(1.0)};
	
	System.out.println("");
	
	for(Figure figure : figures){
		System.out.println(figure.countArea());
		System.out.println(figure.countCircumference());
	}
	
	
}
}
