package figures;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestFigures {
	
	Figure square = new Square(1.0);

	@Test
	public void test() {
		assert square.countCircumference() == 4;
		assert (new Square(2.0)).countCircumference() == 8;
	}

}
