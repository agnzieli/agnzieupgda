package interfaces;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class AnimalExample {

	public static void main(String[] args) {
		
		Animal[] animals = {new Cat(), new Dog()};
		for(Animal animal : animals){
			animal.makeNoise();
		}
		save5NoisesToFile("noises.txt", new Dog());

	}
	
	public static void save5NoisesToFile(String filename, Animal animal){
		try {
			PrintStream out = new PrintStream(filename);
			for (int i = 0; i < 5; i++){
			out.println(animal.makeNoise());
			}
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

}
