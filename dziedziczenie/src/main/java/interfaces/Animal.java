package interfaces;

public interface Animal {
	
	String makeNoise();

}
