package DataProvider;

public interface DataProvider {
	
	int nextInt(int name);
	String nextString(String name);

}
