package animals;

public class Bird extends Animal {

	public Bird(String type) {
		super(type);
	}
	
	@Override
	public void makeNoise(){
		System.out.println("bird-bird");
	}
	
	@Override
	public void eat(){
		System.out.println("A bird (" + this.getType() + ") eats...");
	}
	
	@Override
	public void sleep(){
		System.out.println("A bird (" + this.getType() + ") sleeps...");
	}
	

}
