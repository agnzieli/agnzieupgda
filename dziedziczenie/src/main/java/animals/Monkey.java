package animals;

public class Monkey extends Animal {

	public Monkey(String type) {
		super(type);
	}
	
	public void makeNoise(){
		System.out.println("OuOuOu");
	}
	
	@Override
	public void eat(){
		System.out.println("A monkey (" + this.getType() + ") eats...");
	}
	
	@Override
	public void sleep(){
		System.out.println("A monkey (" + this.getType() + ") sleeps...");
	
	}
}
