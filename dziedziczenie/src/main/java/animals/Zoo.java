package animals;

public class Zoo {
	public static void main(String[] args) {
		Animal ptak1 = new Bird("Wr�bel");
		Animal ptak2 = new Bird("Papuga");
		Animal dog1 = new Dog("Jamnik");
		Cat cat1 = new Pers( "czarny", 1);
		
		Cat cat2 = new Cat("Norweski", "bia�y", 3);
		Animal fish1 = new Fish("Karp");
		Animal monkey1 = new Monkey("Orangutan");
		
		Pers pers = new Pers("rudy", 4);
		
		
		Animal[] animals = {ptak1, ptak2, dog1, cat1, fish1, monkey1};
		
		for(Animal animal : animals){
			animal.makeNoise();
			animal.eat();
			animal.sleep();
		}
		
		cat1.whatAge();
		cat2.whatColour();
		cat2.whatAge();
		pers.eat();
		pers.whatAge();
		pers.whatColour();
	}


}
