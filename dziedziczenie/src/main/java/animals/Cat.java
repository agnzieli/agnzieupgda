package animals;

public class Cat extends Animal {
	
	private String colour;
	private int age;

	public Cat(String type, String colour, int age) {
		super(type);
		this.colour = colour;
		this.age = age;
	}
	
	@Override
	public void makeNoise(){
		System.out.println("miau-miau");
	}
	
	@Override
	public void eat(){
		System.out.println("A cat (" + this.getType() + ") eats...");
	}
	
	@Override
	public void sleep(){
		System.out.println("A cat (" + this.getType() + ") sleeps...");
	
	}
	
	public void whatColour(){
		System.out.println(this.getType() + " is " + this.colour);
	}
	
	public void whatAge(){
		System.out.println(this.getType() + " is " + this.age + " years old.");
	}
}
