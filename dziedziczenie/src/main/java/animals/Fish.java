package animals;

public class Fish extends Animal {

	public Fish(String type) {
		super(type);
	}
	
	@Override
	public void makeNoise(){
		System.out.println("...");
	}
	
	@Override
	public void eat(){
		System.out.println("A fish (" + this.getType() + ") eats...");
	}
	
	@Override
	public void sleep(){
		System.out.println("A fish (" + this.getType() + ") sleeps...");
	
	}
}
