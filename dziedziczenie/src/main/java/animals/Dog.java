package animals;

public class Dog extends Animal {

	public Dog(String type) {
		super(type);
	}
	
	@Override
	public void makeNoise(){
		System.out.println("hau-hau");
	}
	
	@Override
	public void eat(){
		System.out.println("A dog (" + this.getType() + ") eats...");
	}
	
	@Override
	public void sleep(){
		System.out.println("A dog (" + this.getType() + ") sleeps...");
	}

}
