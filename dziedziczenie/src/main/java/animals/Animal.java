package animals;

public abstract class Animal {
	private String type;

	public Animal(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
	public abstract void makeNoise();
	
	public abstract void sleep();
	
	public abstract void eat();
	
	
	
	

}
