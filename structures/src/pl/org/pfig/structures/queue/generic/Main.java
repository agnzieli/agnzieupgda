package pl.org.pfig.structures.queue.generic;

/**
 * Created by RENT on 2017-06-27.
 */
public class Main {
    public static void main(String[] args) {

        SimpleQueue<Integer> queue = new SimpleArrayQueue<>();
        queue.offer(1);
        queue.offer(2);
        queue.offer(3);

        System.out.println(queue.peek());

        while (!queue.isEmpty()){
            System.out.println(queue.poll());
        }
    }
}
