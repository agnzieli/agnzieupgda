package pl.org.pfig.structures.queue.generic;

/**
 * Created by RENT on 2017-06-27.
 */

public interface SimpleQueue<T> {

    boolean isEmpty();
    void offer(T value);
    T poll();
    T peek();
}

