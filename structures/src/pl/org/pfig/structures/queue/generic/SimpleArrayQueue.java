package pl.org.pfig.structures.queue.generic;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */

public class SimpleArrayQueue<T> implements SimpleQueue<T> {

    private int start = 0;
    private int end = 0;
    private Object[] data = new Object[8];
    private boolean isEmpty = true;


    public boolean isEmpty() {
        return isEmpty;
    }

    public void offer(T value) {
        if (isFull()) {

            createNewArray();
        }

        data[end] = value;
        end = (end + 1) % data.length;
        isEmpty = false;
    }

    private void createNewArray() {
        Object[] newData = new Integer[data.length*2];
        int i = 0;
        while (!isEmpty) {
            newData[i] = poll();
            i++;
        }

        data = newData;
        start = 0;
        end = i;
    }

    private boolean isFull() {
        return start == end && !isEmpty;
    }

    public T poll() {
        if (isEmpty) {
            throw new NoSuchElementException("Kolejka jest pusta");
        } else {
            T result = (T) data[start];
            data[start] = null;
            start = (start + 1) % data.length;
            isEmpty = (end == start);
            return result;
        }
    }

    public T peek() {
        if (isEmpty) {
            throw new NoSuchElementException("Kolejka jest pusta");
        } else {
            return (T) data[start];
        }
    }


}

