package pl.org.pfig.structures.queue;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */

public class SimpleArrayQueue implements SimpleQueue {

    private int start = 0;
    private int end = 0;
    private int[] data = new int[8];
    private boolean isEmpty = true;


    public boolean isEmpty() {
        return isEmpty;
    }

    public void offer(int value) {
        if (isFull()) {

            createNewArray();
        }

        data[end] = value;
        end = (end + 1) % data.length;
        isEmpty = false;
    }

    private void createNewArray() {
        int[] newData = new int[data.length*2];
        int i = 0;
        while (!isEmpty) {
            newData[i] = poll();
            i++;
        }

        data = newData;
        start = 0;
        end = i;
    }

    private boolean isFull() {
        return start == end && !isEmpty;
    }

    public int poll() {
        if (isEmpty) {
            throw new NoSuchElementException("Kolejka jest pusta");
        } else {
            int result = data[start];
            start = (start + 1) % data.length;
            isEmpty = (end == start);
            return result;
        }
    }

    public int peek() {
        if (isEmpty) {
            throw new NoSuchElementException("Kolejka jest pusta");
        } else {
            return data[start];
        }
    }


}

