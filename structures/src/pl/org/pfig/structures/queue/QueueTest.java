package pl.org.pfig.structures.queue;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Adrian on 2017-06-27.
 */
public class QueueTest {

    SimpleQueue underTest = new SimpleArrayQueue();

    @Test
    public void offerAnPoll3Elements() {
        underTest.offer(2);
        underTest.offer(4);
        underTest.offer(6);

        assertThat(underTest.isEmpty()).isFalse();
        assertThat(underTest.poll()).isEqualTo(2);
        assertThat(underTest.poll()).isEqualTo(4);
        assertThat(underTest.poll()).isEqualTo(6);
        assertThat(underTest.isEmpty()).isTrue();
    }

    @Test
    public void offerAnPoll2Times() {
        underTest.offer(2);
        underTest.peek();
        underTest.poll();
        underTest.offer(6);

        assertThat(underTest.poll()).isEqualTo(6);
        assertThat(underTest.isEmpty()).isTrue();
    }

    @Test
    public void offerAnPoll100Elements() {
        for (int i = 0; i < 100; i++) {
            underTest.offer(i);
        }

        for (int i = 0; i < 100; i++) {
            assertThat(underTest.poll()).isEqualTo(i);
        }
    }
}