package pl.org.pfig.structures.lists;

/**
 * Created by RENT on 2017-06-23.
 */
public class Main {

    public static void main(String[] args) {

        SimpleList list = new SimpleArrayList();

        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.get(3));

       // list.add(6, 1);
        list.remove(2);

        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.get(3));

    }
}
