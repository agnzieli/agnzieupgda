package pl.org.pfig.structures.lists;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-26.
 */
public class SimpleLinkedList implements SimpleList {

    private Element first;
    private Element last;
    private int number = 0;

    @Override
    public void add(int value) {
        Element element = new Element();
        element.value = value;

        if(first == null){
            first = element;
        }else{
            last.next = element;
            element.prev = last;

        }
        last = element;
        number++;
    }

    @Override
    public int get(int index) {
        Element element = first;
        if (index < number) {
            for (int i = 0; i < number; i++) {
                element = element.next;
            }
            return element.value;
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void add(int value, int index) {

        Element el_f = first;
        Element element = new Element();
        element.value = value;

        if (index == 0) {
            element.next = first;
            first = element;
            if (number == 0) {
                last = element;
            }
        } else if (index == number){
            add(element.value);
        } else if (index < number) {
            for (int i = 0; i < index; i++) {
                el_f = el_f.next;
            }
            Element next = element;
            Element prev = element.prev;
            element.prev = prev;
            element.next = next;
            prev.next = element;
            next.prev = element;

        } else {
            throw new IllegalArgumentException();
        }
        number++;
    }

    @Override
    public boolean contain(int value) {
        Element el = first;
        for (int i = 0; i < number; i++){
            if (el.value == value){
                return true;
            }
            el = el.next;
        }
        return false;
    }

    @Override
    public void remove(int index) {
        Element el_f = first;
        Element el_l = last;
        if (index == 0){
            first = el_f.next;

        } else if (index == number){
            last = el_l.prev;
        }


    }

    @Override
    public void removeValue(int value) {

    }

    @Override
    public int size() {
        return number;
    }

    private static class Element {
        int value;
        Element next;
        Element prev;


    }
}
