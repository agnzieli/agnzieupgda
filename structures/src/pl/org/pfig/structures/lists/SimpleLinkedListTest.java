package pl.org.pfig.structures.lists;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by RENT on 2017-06-26.
 */
public class SimpleLinkedListTest {

    private SimpleLinkedList underList = new SimpleLinkedList();

    @Test
    public void add() throws Exception {
        underList.add(4);

        assertThat(underList.contain(4)).isTrue();
        assertThat(underList.contain(10)).isFalse();
        assertThat(underList.get(0)).isEqualTo(4);
        assertThat(underList.size()).isEqualTo(1);
    }

    @Test
    public void get() throws Exception {
    }

    @Test
    public void add1() throws Exception {
    }

    @Test
    public void contain() throws Exception {
    }

    @Test
    public void remove() throws Exception {
    }

    @Test
    public void removeValue() throws Exception {
    }

    @Test
    public void size() throws Exception {
    }

}