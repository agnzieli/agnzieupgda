package pl.org.pfig.structures.lists;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-23.
 */
public class SimpleArrayList implements SimpleList{

    int number = 0;
    int[] data = new int[100];

    @Override
    public int get(int index) {

        if (index < number) {
            return data[index];
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void add(int value) {
        if (number < data.length) {
            data[number] = value;
            number++;
        } else {
            arrayTooSmall();
            return;
        }
    }

    private void arrayTooSmall() {
        throw new RuntimeException("Za mało miejsca na liście.");
    }

    @Override
    public void add(int value, int index) {
        if (number < data.length) {
            if (index < number) {
                for (int i = number; i > index; i--) {
                    data[i] = data[i - 1];
                }
                data[index] = value;
            } else {
                throw new IndexOutOfBoundsException();
            }
            number++;
        } else throw new IllegalArgumentException();
    }

    @Override
    public boolean contain(int value) {
        for (int i : data){
            if (i == value){
                return true;
            }
        }
        return false;
    }

    @Override
    public void remove(int index) {

        if (index < number && index >= 0) {
            for (int i = index; i < number-1; i++) {
                data[i] = data[i + 1];
            }
            number--;
        } else {
            throw new IndexOutOfBoundsException("Zły indeks");
        }
    }

    @Override
    public void removeValue(int value) {
        for (int i = 0; i < number; i++) {
            if (data[i] == value) {
                remove(i);
                number--;
                break; //lub return
            }
        }

    }

    @Override
    public int size() {
        return number;
    }
}
