package ListMath;

import java.util.List;

public class ListMath {
	
//	public static int sum(List<Integer> numbers){
//		
//		int sum = 0;
//		for(int i = 0; i < numbers.size(); i++){
//			sum += numbers.get(i);			
//		}
//		return sum;
//	}
	
	public static int sum (List<Integer> numbers){
		int sum = 0;
		for (int i : numbers){
			sum += i;
		}
		return sum;		
	}
	
//	public static int product(List<Integer> numbers){
//		
//		int result = 1;
//		for ( int i = 0; i < numbers.size(); i++){
//			result *= numbers.get(i);
//		}
//		return result;
//	}
	
	public static int product(List<Integer> numbers){
		int result = 1;
		for (int i : numbers){
			result *= i;
		}
		return result;		
	}
	
	public static double mean(List<Integer> numbers){
		
		return (double) sum(numbers)/numbers.size();
	}

}
