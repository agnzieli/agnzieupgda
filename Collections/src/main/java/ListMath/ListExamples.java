package ListMath;

import java.util.ArrayList;
import java.util.List;

public class ListExamples {
	
	public static void main(String[] args) {
		
		List <Integer> numbers = new ArrayList<Integer>();
		numbers.add(2);
		numbers.add(4);
		numbers.add(6);
		numbers.add(1);
		
		System.out.println("Ca�a lista:");
		for (int i : numbers){
			System.out.println(i);
		}
		
		System.out.println("Element o indeksie 2:");
		System.out.println(numbers.get(2));
		
		
		numbers.remove((Integer) 6);
		
		numbers.add(1, 100);
		
		System.out.println("Ca�a lista:");
		for (int i : numbers){
			System.out.println(i);
		}
		
		System.out.println("Rozmiar listy:");
		System.out.println(numbers.size());
		
		numbers.stream().forEach(System.out::println);
		
		
		int s= numbers.stream().mapToInt((i) -> i).sum();
	}

}
