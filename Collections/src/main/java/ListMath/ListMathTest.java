package ListMath;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ListMathTest {

	@Test
	public void sumTest() {
		List<Integer> numbers = new ArrayList();
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		
		assert ListMath.sum(numbers) == 15;
		//assert ListMath.sum(numbers) == 10;
		
		assert ListMath.product(numbers) == 120;
		
		assert ListMath.mean(numbers) == 5;
		//assert ListMath.mean(numbers) == 4;
	
	}

}
