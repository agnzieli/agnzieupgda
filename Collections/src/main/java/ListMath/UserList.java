package ListMath;

import java.util.ArrayList;
import java.util.List;

public class UserList {

	public static void main(String[] args) {

		List<User> users = new ArrayList<>();
		users.add(new User("admin", "12345"));
		users.add(new User("Ania", "lkspad"));
		
		for (User user : users) {
			System.out.println(user);
		}

	}

}
