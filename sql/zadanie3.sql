DROP DATABASE IF EXISTS company;

CREATE DATABASE company;
USE company;

CREATE TABLE worker(
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(40),
salary DOUBLE,
age INT);

INSERT INTO worker (name, salary, age) VALUES
("Jan Kowalski", 1000, 21),
("Maria Nowa", 1000, 67),
("Karolina Wozniak", 1000, 19),
("Marcin Kot", 1500, 32),
("Paweł Szymanski", 1500, 56),
("Piotr Piotrowski", 1500, 29),
("Anna Mocna", 2000, 21),
("Monika Garnek", 2000, 35),
("Mikołaj Ptak", 4000, 21),
("Szymon Zalewski", 4000, 28);

SELECT*FROM worker;

SELECT avg(salary) FROM worker;
SELECT max(salary) FROM worker;
SELECT min(salary) FROM worker;

SELECT count(*), salary FROM worker GROUP BY salary;

SELECT avg(salary), age FROM worker GROUP BY age;

SELECT*FROM worker WHERE salary = (SELECT max(salary) FROM worker);
SELECT*FROM worker WHERE salary = (SELECT min(salary) FROM worker);
SELECT*FROM worker WHERE salary IN (SELECT min(salary) FROM worker);

SELECT*FROM worker ORDER BY salary;
SELECT*FROM worker ORDER BY salary DESC;

SELECT*FROM worker ORDER BY salary LIMIT 1;