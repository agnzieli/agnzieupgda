SET SQL_SAFE_UPDATES = 0;

DROP DATABASE IF EXISTS personal_data;

CREATE DATABASE personal_data;
USE personal_data;


CREATE TABLE person(
first_name VARCHAR(20) ,
last_name VARCHAR(20) ,
age INT);

INSERT INTO person VALUES(
"Anna", "Kowalska", 25);

INSERT INTO person VALUES(
"Kacper", "Piechocki", 19);

INSERT INTO person VALUES(
"Monika", "Mazowiecka", 26);

INSERT INTO person VALUES(
"Karolina", "Pasek", 25);

INSERT INTO person VALUES(
"Filip", "Rozner", 19);

SELECT first_name FROM person;

SELECT COUNT(*)FROM person;

SELECT count(*), age FROM person GROUP BY age;

UPDATE person SET age = age + 1;

SELECT*FROM person;






