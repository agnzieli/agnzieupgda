DROP DATABASE IF EXISTS shop;

CREATE DATABASE shop;
USE shop;

CREATE TABLE category(
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30),
description TEXT);

CREATE TABLE product(
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30),
price DOUBLE,
category_id INT,
FOREIGN KEY (category_id) REFERENCES category(id));

insert into category (name, description) VALUES
("pieczywo", "bułki, chleby, inne wypieki"),
("słodycze", "słodziutkie słodycze"),
("warzywa i owoce", "samo zdrowie");

select*from product;

insert into product (name, price, category_id) VALUES
("chleb razowy", 2.99, 1),
("bułka żytnia", 1.00, 1),
("ciabatta", 0.99, 1),
("snickers", 2.30, 2),
("czekolada gorzka", 2.76, 2),
("torcik wedlowski", 12.00, 2),
("awokado", 4.89, 3),
("jabłko", 0.50, 3),
("pomarańcza", 0.80, 3),
("marchew", 1.50, 3),
("sałata", 2.30, 3);

select*from product;

SELECT product.name, category.name FROM product LEFT JOIN category ON product.category_id = category.id;

SELECT category.name, count(*) FROM product LEFT JOIN category ON product.category_id = category.id GROUP BY category_id; 

SELECT avg(p.price), c.name FROM product p LEFT JOIN category c ON p.category_id = c.id GROUP BY category_id;

SELECT p.id, p.name, p.price, c.name FROM product p LEFT JOIN category c ON p.category_id = c.id;  

CREATE VIEW view2 (id, produkt, cena, kategoria) AS SELECT p.id, p.name, p.price, c.name FROM product p LEFT JOIN category c ON p.category_id = c.id; 

SELECT * FROM view2;

CREATE VIEW view AS SELECT p.id, p.name AS product_name, p.price, c.name FROM product p LEFT JOIN category c ON p.category_id = c.id; 

SELECT * FROM view;