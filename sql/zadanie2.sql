DROP DATABASE IF EXISTS contacts;
CREATE DATABASE contacts;
USE contacts;

CREATE TABLE contact(
id INT AUTO_INCREMENT PRIMARY KEY,
first_name VARCHAR(30),
last_name VARCHAR(30),
phone_number VARCHAR(9),
email VARCHAR(40)
);

INSERT INTO contact (first_name, last_name, phone_number, email) VALUES
("Anna", "Nowak", "345652345", "anna@wp.pl"),
("Magda", "Kowalska", "346532540", "Magda@onet.pl"),
("Adam", "Puchowski", "875324682", "adam@gmail.com"),
("Jan", "Janowski", "654323578", "janek@o2.pl"),
("Karol", "Stankiewicz", "876812359", "karolek@wp.pl");

INSERT INTO contact (first_name, last_name, phone_number) VALUES
("Maria", "Dabrowska", "123456789");

SELECT*FROM contact;

SELECT last_name, phone_number FROM contact;

SELECT COUNT(*) FROM contact;

UPDATE contact SET email = "brak" WHERE email IS NULL;

SELECT concat(first_name, " ", last_name) AS name, email FROM contact;

SELECT phone_number FROM contact WHERE phone_number LIKE "34_______";

SELECT count(*), phone_number FROM contact WHERE phone_number LIKE "________0" GROUP BY phone_number;






