DROP DATABASE IF EXISTS geo;

CREATE DATABASE geo;
USE geo;

CREATE TABLE city(
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30),
citizens MEDIUMINT
);

CREATE TABLE state(
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30),
population BIGINT,
capital_id INT,
FOREIGN KEY (capital_id) REFERENCES city(id));

INSERT INTO city VALUES
(id, "Warszawa", 1753977),
(id, "Gdańsk", 463754),
(id, "Berlin", 3400000),
(id, "Monachium", 1429584),
(id, "Praga", 1280508),
(id, "Brno", 404820),
(id, "Paryż", 2243833),
(id, "Amsterdam", 801200),
(id, "Bruksela", 177849),
(id, "Barcelona", 1611822);

INSERT INTO state VALUES
(id, "Polska", 38432992, 1),
(id, "Niemcy", 80722792, 3),
(id, "Czechy", 10541466, 5),
(id, "Holandia", 17108799, 8),
(id, "Belgia", 11303528, 9);

SELECT state.name, city.name FROM state LEFT JOIN city ON state.capital_id = city.id;

SELECT city.name FROM city INNER JOIN state ON city.id = state.capital_id;

SELECT state.name, city.citizens AS capital_citizens FROM state LEFT JOIN city ON city.id = state.capital_id;

SELECT state.name FROM state INNER JOIN city ON state.capital_id = city.id WHERE (city.citizens > 0.1 * state.population);

ALTER TABLE city ADD is_capital BOOLEAN DEFAULT 0;

UPDATE city 
INNER JOIN state ON 
state.capital_id = city.id 
SET is_capital = 1;



SELECT*FROM city;
