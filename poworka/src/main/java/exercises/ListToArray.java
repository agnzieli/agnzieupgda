package exercises;

import java.util.Arrays;
import java.util.List;

/**
 * Created by RENT on 2017-06-13.
 */
public class ListToArray {

    public static void main(String[] args) {

        for(int i : toArray(Arrays.asList(1,2,3,4,5,6))){
            System.out.println(i);
        }

    }

    public static int[] toArray(List<Integer> list){
        int[] array = new int[list.size()];
        for (int i = 0; i < list.size(); i++){
            array[i] = list.get(i);
        }

        return array;
    }
}
