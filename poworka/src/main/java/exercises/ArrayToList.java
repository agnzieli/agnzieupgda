package exercises;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-06-13.
 */
public class ArrayToList {
    public static void main(String[] args) {

        for(int i : toList(new Integer[] {1,2,3,4,5,6,7})){
            System.out.println(i);
        }

    }

    //generyki
   public static <T> List<T> toList (T[] array){
       List list = new ArrayList<Integer>();
       for (int i = 0; i < array.length; i++){
           list.add(array[i]);

     }

//    public static List<Object> toList (Object[] array) {
//    }

       return list;

   }
}
