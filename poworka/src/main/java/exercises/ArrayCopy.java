package exercises;

/**
 * Created by RENT on 2017-06-13.
 */
public class ArrayCopy {
    public static void main(String[] args) {

        for(int i : copy(new int[] {1,2,3,4,5,6,7,8,9}))
        System.out.println(i);
    }

    public static int[] copy (int[] array){
        int[] arrayCopy = new int[array.length];
        for (int i = 0; i < array.length; i++){
            arrayCopy[i] = array[i];
        }
        return arrayCopy;
    }


    }

