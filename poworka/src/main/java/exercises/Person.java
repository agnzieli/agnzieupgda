package exercises;

import java.util.Random;


public class Person {

    private String name;

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    private String surname;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Person randomPerson() {

        Random r = new Random();
        String name = "";
        String surname = "";
        int a = r.nextInt(11);
        int b = r.nextInt(21);
        for (int i = 0; i < a; i++){
            name += (char)r.nextInt(26)+97;
        }
        for (int i = 0; i < b; i++){
            surname += (char)r.nextInt(26)+97;
        }

        return new Person(name, surname);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    public static void main(String[] args) {
        System.out.println(randomPerson());
    }
}
