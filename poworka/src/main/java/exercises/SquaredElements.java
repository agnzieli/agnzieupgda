package exercises;

/**
 * Created by RENT on 2017-06-13.
 */
public class SquaredElements {
    public static void main(String[] args) {

        for (int i : squared(new int[] {1,2,3,4,5})){
            System.out.println(i);
        }

    }

    public static int[] squared (int[] array){
        int[] sq = new int[array.length];
        for (int i = 0; i < array.length; i++){
            sq[i] = array[i]*array[i];
        }
        return sq;
    }
}
