package exercises;

/**
 * Created by RENT on 2017-06-13.
 */
public class LetterInString {
    public static void main(String[] args) {
        System.out.println(count("hhhhh", 'h'));
    }


    public static int count(String text, char letter) {
        int count = 0;

        for (int i = 0; i < text.toCharArray().length; i++) {
            if (text.toCharArray()[i] == letter) {
                count += 1;
            }
        }
        return count;
    }


}
