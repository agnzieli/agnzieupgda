package exercises;

/**
 * Created by RENT on 2017-06-13.
 */
public class PrimeNumbers {

    public static void main(String[] args) {
        System.out.println( prime(7));
    }

    public static boolean prime (int a){
        if (a < 2){
            return false;
        }
        for (int i = 2; i * i <=a ; i++ ) {
            if (a % i == 0)
                return false;
        }

            return true;
    }
}
