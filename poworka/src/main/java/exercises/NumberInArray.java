package exercises;

/**
 * Created by RENT on 2017-06-13.
 */
public class NumberInArray {

    public static void main(String[] args) {
        int[] array = new int[]{1,2,3,1,2,1,1,5,6,6,5,4,8,9,0};
        System.out.println(howMany(array, 1));
    }


    public static int howMany(int[] array, int a){
        int count = 0;
        for (int i = 0; i<array.length; i++){
            if (array[i] == a){
                count += 1;
            }
        }
        return count;
    }


}
