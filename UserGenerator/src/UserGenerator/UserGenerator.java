package UserGenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {
	private final String path = "resources/";
	
	public User getRandomUser(){
		UserSex us = UserSex.SEX_MALE;
		if (new Random().nextInt(2) == 0){
			us = UserSex.SEX_FEMALE;
		}
		return getRandomUser(us);
	}
	
	public User getRandomUser(UserSex sex){
		User currentUser = new User();
		currentUser.setSex(sex);
		String name = "";
		if(sex.equals(UserSex.SEX_MALE)){
			name = getRandomLineFromFile("name_m.txt");
		} else {
			name = getRandomLineFromFile("name_f.txt");
		}
		currentUser.setName(name);
		String lastname = "";
		lastname = getRandomLineFromFile("lastname.txt");
		currentUser.setSecondName(lastname);
		String[] phone = new String[9];
		String phoneNum = "";
		for (int i = 0; i < phone.length; i++){
			phone[i] = Integer.toString(new Random().nextInt(10));
			if (phone[0].equals("0")){
				phone[0] = Integer.toString(new Random().nextInt(8) + 1);
			}
			phoneNum += phone[i];
		}
		currentUser.setPhone(phoneNum);
		
		Address adr = getRandomAddress();
		currentUser.setAddress(adr);
		
		String[] ccn = new String[4];
		String ccnNum = "";
		for (int i = 0; i < 4; i ++){
			for (int j = 0; j < ccn.length; j++){
				ccn[j] = Integer.toString(new Random().nextInt(10));
				ccnNum += ccn[j];
			}
			ccnNum += " ";
			
		}
		
		currentUser.setCCN(ccnNum);
		
		//String[] pesel = new String [11];
				
		String dt = getRandomBirthDate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		try {
			currentUser.setBirthDate(sdf.parse(dt));
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}
		
		String[] dropped = dt.split("");
		String day = dropped[0] + dropped[1];
		String month = dropped[3] + dropped[4];
		String year = dropped[8] + dropped[9];
		
		int m = Integer.parseInt(month);
		String newYear = dt.substring(6);
		int y = Integer.parseInt(year);
		
		if(y >= 1800 && y <= 1899){
			m += 80;
		}
		if(y >=2000 && y <= 2099){
			m += 20;
		}
		
		
		String p = "";
		for (int i = 0; i < 3; i++){
			p = Integer.toString(new Random().nextInt(10) );
			p += p;
		}
		
		int r = new Random().nextInt(4);
		if (sex == UserSex.SEX_FEMALE){
			String[] val = {"2","4","6","8","0"};
			p = p + val[r];
			} 
		
			if (sex == UserSex.SEX_MALE){
				String[] val = {"1","3","5","7","9"};
				p = p + val[r];
			}
			
		String pesel = year + month + day + p  ;
		currentUser.setPESEL(pesel);
		
		return currentUser;
	}
	
	private String getRandomBirthDate(){
		int[] daysInMonths = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		int year = 1890 + new Random().nextInt(120);
		int month = new Random().nextInt(12) + 1;
		if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0){
		daysInMonths[1]++;
		}
		int day = new Random().nextInt(daysInMonths[month - 1]) + 1;
		return leadZero(day) + "." + leadZero(month) + "." + year;
		
	}
	
	private String leadZero(int arg){
		if(arg < 10) return "0" + arg;
		else return "" + arg;
	}

	private Address getRandomAddress() {
		Address adr = new Address();
		String[] cityData = getRandomLineFromFile("city.txt").split(" \t ");
		adr.setCountry("Poland");
		adr.setCity(cityData[0]);
		adr.setZipcode(cityData[1]);
		adr.setStreet("ul. " + getRandomLineFromFile("street.txt"));
		adr.setNumber("" + new Random().nextInt(101));
		return adr;
	}
	
	public int countLines(String filename){
		int lines = 1;
		try(Scanner sc = new Scanner(new File(path + filename))) {
			while(sc.hasNextLine()){
				lines++;
				sc.nextLine();
			}
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return lines;
	}
	
	public String getRandomLineFromFile(String filename){
		int allLines = countLines(filename);
		int line = new Random().nextInt(allLines) + 1;
		int currLine = 1;
		String currentLineContent = "";
		try(Scanner sc = new Scanner(new File(path + filename))){
			while(sc.hasNextLine()){
				currLine++;
				currentLineContent = sc.nextLine();
				if(line == currLine) {
					return currentLineContent;
				}
			}
		} catch (FileNotFoundException e){
			System.out.println(e.getMessage());
		}
		return null;
		
	
	}

}
