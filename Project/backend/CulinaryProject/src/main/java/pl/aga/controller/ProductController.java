package pl.aga.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.aga.entity.Ingredient;
import pl.aga.entity.Product;
import pl.aga.entity.Recipe;
import pl.aga.repository.IngredientRepository;
import pl.aga.repository.ProductRepository;
import pl.aga.repository.RecipeRepository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private IngredientRepository ingredientRepository;

    @RequestMapping("/show")
    public List<Product> listProducts() {
        return productRepository.findAll();
    }

    @RequestMapping("/add")
    public Product addProduct (@RequestParam(name = "name") String name,
                               @RequestParam(name = "kcal") String kcal,
                               @RequestParam(name = "carbo") String carbo,
                               @RequestParam(name = "fat") String fat,
                               @RequestParam(name = "protein") String protein,
                               @RequestParam(name = "description") String description){

        if (productRepository.findByName(name).size() == 0) {
            Product p = new Product();
            p.setName(name);
            p.setKcal(Double.valueOf(kcal));
            p.setCarbo(Double.valueOf(carbo));
            p.setFat(Double.valueOf(fat));
            p.setProtein(Double.valueOf(protein));
            p.setDescription(description);
            return productRepository.save(p);
        }
        return null;
    }


    @RequestMapping("/show/{id}")
    public Product showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return productRepository.findOne(myId);
    }

    @RequestMapping("/edit/{id}")
    public Product editProduct(@PathVariable(name = "id") String id,
                               @RequestParam(name = "name")String name,
                               @RequestParam(name = "kcal")String kcal,
                               @RequestParam(name = "carbo")String carbo,
                               @RequestParam(name = "fat")String fat,
                               @RequestParam(name= "protein")String protein,
                               @RequestParam(name = "description")String desc){
        long myId = Long.valueOf(id);
        Product p = productRepository.findOne(myId);
        p.setName(name);
        p.setKcal(Double.valueOf(kcal));
        p.setCarbo(Double.valueOf(carbo));
        p.setFat(Double.valueOf(fat));
        p.setProtein(Double.valueOf(protein));
        p.setDescription(desc);
        return productRepository.save(p);
    }

    @RequestMapping("/delete/{id}")
    public boolean deleteProduct(@PathVariable("id") long id){

        for(Recipe r : recipeRepository.findByProductId(id)) {
            recipeRepository.delete(r);
        }
        Product p = productRepository.findOne(id);
        List<Ingredient> i = new LinkedList<>();
        p.setIngredients(i);
        productRepository.delete(p);
        return true;
    }

    @RequestMapping("/recipes/{id}")
    public List<Recipe> showConnectedRecipes(@PathVariable(name = "id") String id){
        long myId = Long.valueOf(id);
        List<BigInteger> list = productRepository.findById(myId);
        List<Long> myList = new LinkedList<>();
        for(BigInteger bi : list){
            Long l = bi.longValue();
            myList.add(l);
        }
        List<Recipe> recipes = new LinkedList<>();
        for(Long l : myList){
            Recipe r = recipeRepository.findOne(l);
            recipes.add(r);
        }
        return recipes;
    }


}
