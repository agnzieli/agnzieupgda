package pl.aga.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.aga.entity.Product;
import pl.aga.entity.Recipe;

import java.math.BigInteger;
import java.util.List;

public interface RecipeRepository extends CrudRepository<Recipe, Long>, JpaRepository<Recipe, Long> {

    @Query(value="SELECT id FROM recipe WHERE id IN (SELECT recipe_id FROM INGREDIENT WHERE product_id IN (SELECT id FROM product WHERE name LIKE %:prodName%))", nativeQuery = true)
    List<BigInteger> findByProductNameContaining(@Param("prodName") String prodName);

    @Query(value="SELECT * FROM recipe WHERE id IN (SELECT recipe_id FROM ingredient WHERE product_id = :prodId)", nativeQuery = true)
    List<Recipe> findByProductId(@Param("prodId") long prodId);

    @Query(value="SELECT id FROM recipe WHERE name LIKE %:recipeName%", nativeQuery = true)
    List<BigInteger> findByNameContaining(@Param("recipeName") String recipeName);

    @Query(value="SELECT id FROM recipe WHERE UPPER(name) = UPPER(?1)", nativeQuery = true)
    List<BigInteger> findByName(String name);


}
