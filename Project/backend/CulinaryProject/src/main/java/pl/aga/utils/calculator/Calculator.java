package pl.aga.utils.calculator;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static pl.aga.utils.calculator.Product.*;
import static pl.aga.utils.calculator.Unit.*;

/**
 * Created by RENT on 2017-08-02.
 */
@CrossOrigin
@RestController
@RequestMapping("/calculator")
public class Calculator {
    private Product product;
    private double amount;
    private Unit unit;
    private Unit from;
    private Unit to;

    public Calculator(Product product, double amount, Unit unit, Unit from, Unit to) {
        this.product = product;
        this.amount = amount;
        this.unit = unit;
        this.from = from;
        this.to = to;
    }

    public Calculator() {
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Unit getFrom() {
        return from;
    }

    public void setFrom(Unit from) {
        this.from = from;
    }

    public Unit getTo() {
        return to;
    }

    public void setTo(Unit to) {
        this.to = to;
    }

    //@RequestMapping(method = RequestMethod.GET)
    @RequestMapping("/calculate")
    public double calculate( @RequestParam(name = "product") String p,
                             @RequestParam(name = "from") String fr,
                             @RequestParam(name = "to") String t,
                             @RequestParam(name = "amount") double amount) {
        Product product = Product.valueOf(p);
        Unit from = Unit.valueOf(fr);
        Unit to = Unit.valueOf(t);

        double result = 0;

        if (product.equals(WATER)) {
            if (from.equals(GLASS)) {
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(SPOON, 16.68);
                predictor.put(TEASPOON, 50.0);
                predictor.put(GRAM, 250.0);
                predictor.put(MILLIGRAM, 25000.0);
                predictor.put(LITER, 0.25);
                predictor.put(MILLILITER, 250.0);

                result = amount * predictor.get(to);
            }
            if (from.equals(SPOON)){
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(GLASS, 0.06);
                predictor.put(TEASPOON, 3.0);
                predictor.put(GRAM, 15.0);
                predictor.put(MILLIGRAM, 1500.0);
                predictor.put(LITER, 0.01);
                predictor.put(MILLILITER, 15.0);
                result = amount * predictor.get(to);
            }
            if (from.equals(TEASPOON)) {
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(GLASS, 0.02);
                predictor.put(SPOON, 0.33);
                predictor.put(GRAM, 5.0);
                predictor.put(MILLIGRAM, 500.0);
                predictor.put(LITER, 0.01);
                predictor.put(MILLILITER, 5.0);
                result = amount * predictor.get(to);
            }
            if (from.equals(GRAM)) {
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(GLASS, 0.0);
                predictor.put(SPOON, 0.07);
                predictor.put(TEASPOON, 0.2);
                predictor.put(MILLIGRAM, 100.0);
                predictor.put(LITER, 0.0);
                predictor.put(MILLILITER, 1.0);
                result = amount * predictor.get(to);
            }
//            if (from.equals(MILLIGRAM)) {
//                wiadomość, nie przeliczamy tego
//            }
            if (from.equals(LITER)) {
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(GLASS, 4.0);
                predictor.put(SPOON, 66.7);
                predictor.put(GRAM, 1000.0);
                predictor.put(MILLIGRAM, 100000.0);
                predictor.put(TEASPOON, 200.0);
                predictor.put(MILLILITER, 1000.0);
                result = amount * predictor.get(to);
            }
//            if(from.equals(MILLILITER)){
//                wiadomosc, nie przeliczamy tego
//            }
        }
        if (product.equals(FLOUR)) {
            if (from.equals(GLASS)) {
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(SPOON, 16.67);
                predictor.put(TEASPOON, 50.0);
                predictor.put(GRAM, 167.0);
                predictor.put(MILLIGRAM, 16700.0);
                predictor.put(LITER, 0.25);
                predictor.put(MILLILITER, 250.0);
                result = amount * predictor.get(to);
            }
            if (from.equals(SPOON)){
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(GLASS, 0.06);
                predictor.put(TEASPOON, 3.0);
                predictor.put(GRAM, 10.0);
                predictor.put(MILLIGRAM, 1000.0);
                predictor.put(LITER, 0.01);
                predictor.put(MILLILITER, 15.0);
                result = amount * predictor.get(to);
            }
            if (from.equals(TEASPOON)) {
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(GLASS, 0.02);
                predictor.put(SPOON, 0.33);
                predictor.put(GRAM, 3.33);
                predictor.put(MILLIGRAM, 333.0);
                predictor.put(LITER, 0.01);
                predictor.put(MILLILITER, 5.0);
                result = amount * predictor.get(to);
            }
            if (from.equals(GRAM)) {
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(GLASS, 0.01);
                predictor.put(SPOON, 0.01);
                predictor.put(TEASPOON, 0.3);
                predictor.put(MILLIGRAM, 100.0);
                predictor.put(LITER, 0.0);
                predictor.put(MILLILITER, 1.5);
                result = amount * predictor.get(to);
            }
//            if (from.equals(MILLIGRAM)) {
//                wiadomość, nie przeliczamy tego
//            }
            if (from.equals(LITER)) {
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(GLASS, 4.0);
                predictor.put(SPOON, 66.7);
                predictor.put(GRAM, 666.67);
                predictor.put(MILLIGRAM, 66667.0);
                predictor.put(TEASPOON, 200.0);
                predictor.put(MILLILITER, 1000.0);
                result = amount * predictor.get(to);
            }
//            if(from.equals(MILLILITER)){
//                wiadomosc, nie przeliczamy tego
//            }
        }
        if (product.equals(SUGAR)) {
            if (from.equals(GLASS)) {
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(SPOON, 17.0);
                predictor.put(TEASPOON, 51.0);
                predictor.put(GRAM, 203.25);
                predictor.put(MILLIGRAM, 20325.0);
                predictor.put(LITER, 0.25);
                predictor.put(MILLILITER, 250.0);
                result = amount * predictor.get(to);
            }
            if (from.equals(SPOON)){
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(GLASS, 0.06);
                predictor.put(TEASPOON, 3.0);
                predictor.put(GRAM, 12.0);
                predictor.put(MILLIGRAM, 1200.0);
                predictor.put(LITER, 0.01);
                predictor.put(MILLILITER, 15.0);
                result = amount * predictor.get(to);
            }
            if (from.equals(TEASPOON)) {
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(GLASS, 0.02);
                predictor.put(SPOON, 0.33);
                predictor.put(GRAM, 4.0);
                predictor.put(MILLIGRAM, 400.0);
                predictor.put(LITER, 0.01);
                predictor.put(MILLILITER, 5.0);
                result = amount * predictor.get(to);
            }
            if (from.equals(GRAM)) {
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(GLASS, 0.0);
                predictor.put(SPOON, 0.08);
                predictor.put(TEASPOON, 0.25);
                predictor.put(MILLIGRAM, 100.0);
                predictor.put(LITER, 0.0);
                predictor.put(MILLILITER, 1.0);
                result = amount * predictor.get(to);
            }
//            if (from.equals(MILLIGRAM)) {
//                wiadomość, nie przeliczamy tego
//            }
            if (from.equals(LITER)) {
                Map<Unit, Double> predictor = new HashMap<>();
                predictor.put(GLASS, 4.0);
                predictor.put(SPOON, 66.7);
                predictor.put(GRAM, 813.0);
                predictor.put(MILLIGRAM, 81300.0);
                predictor.put(TEASPOON, 200.0);
                predictor.put(MILLILITER, 1000.0);
                result = amount * predictor.get(to);
            }
//            if(from.equals(MILLILITER)){
//                wiadomosc, nie przeliczamy tego
//            }
        }

        return  result;
    }
}
