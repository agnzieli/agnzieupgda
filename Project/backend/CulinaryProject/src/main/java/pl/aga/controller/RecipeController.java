package pl.aga.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.aga.entity.Ingredient;
import pl.aga.entity.IngredientId;
import pl.aga.entity.Product;
import pl.aga.entity.Recipe;
import pl.aga.repository.ProductRepository;
import pl.aga.repository.RecipeRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/recipes")
public class RecipeController {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private RecipeRepository recipeRepository;

    @RequestMapping("/")
    public String recipe() {
        return "";
    }

    @RequestMapping("/show")
    public List<Recipe> listRecipes() {
        return recipeRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Recipe showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return recipeRepository.findOne(myId);
    }

    @RequestMapping("/add")
    public Recipe addRecipe(@RequestParam("name") String name) {
        if (recipeRepository.findByName(name).size() == 0) {
            return recipeRepository.save(new Recipe(name));

        }
        return null;
    }

    @RequestMapping("/add/recipe/{recipeId}/product/{productId}/amount/{amount}")
    public Recipe addRecipe(@PathVariable("recipeId") long recipeId,
                            @PathVariable("productId") long productId,
                            @PathVariable("amount") double amount) {
        Recipe recipe = recipeRepository.findOne(recipeId);
        Product product = productRepository.findOne(productId);

        recipe.getIngredients().add(new Ingredient(new IngredientId(recipe, product), amount));
        return recipeRepository.save(recipe);
    }

    @RequestMapping("/add/{recipeId}")
    public Recipe addRecipe(@PathVariable("recipeId") long recipeId,
                            @RequestParam("description") String desc,
                            @RequestParam("type") String type) {
        Recipe recipe = recipeRepository.findOne(recipeId);
        recipe.setDescription(desc);
        recipe.setType(type);
        return recipeRepository.save(recipe);
    }

    @RequestMapping("/edit/{id}")
    public Recipe editRecipe(@PathVariable("id") String id,
                             @RequestParam("name") String name) {
        Long myId = Long.valueOf(id);
        Recipe r = recipeRepository.findOne(myId);
        r.setName(name);
        return recipeRepository.save(r);
    }

    @RequestMapping("/edit/recipe/{recipeId}/product/{productId}/amount/{amount}")
    public Recipe editRecipe(@PathVariable("recipeId") long recipeId,
                             @PathVariable("productId") long productId,
                             @PathVariable("amount") double amount) {
        Recipe r = recipeRepository.findOne(recipeId);
        Product p = productRepository.findOne(productId);
        r.getIngredients().removeAll(r.getIngredients());
        r.getIngredients().add(new Ingredient(new IngredientId(r, p), amount));

        return recipeRepository.save(r);
    }

    @RequestMapping("/edit/details/{recipeId}")
    public Recipe editRecipe(@PathVariable("recipeId") long recipeId,
                             @RequestParam("description") String desc,
                             @RequestParam("type") String type) {
        Recipe r = recipeRepository.findOne(recipeId);
        r.setDescription(desc);
        r.setType(type);
        return recipeRepository.save(r);
    }



    @RequestMapping("/delete/{id}")
    public boolean deleteRecipe (@PathVariable("id") String id){
        long myId = Long.valueOf(id);
        recipeRepository.delete(myId);
        return true;
    }
}