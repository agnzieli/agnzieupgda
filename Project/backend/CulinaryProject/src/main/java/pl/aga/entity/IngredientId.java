package pl.aga.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by RENT on 2017-08-03.
 */
@Embeddable
public class IngredientId implements Serializable {
    @ManyToOne
    private Recipe recipe;
    @ManyToOne
    @JsonBackReference
    private Product product;

    public IngredientId() {
    }

    public IngredientId(Recipe recipe, Product product) {
        this.recipe = recipe;
        this.product = product;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
