package pl.aga.repository;

import org.springframework.data.repository.CrudRepository;
import pl.aga.entity.Ingredient;

/**
 * Created by RENT on 2017-08-08.
 */
public interface IngredientRepository extends CrudRepository<Ingredient, Long> {
}
