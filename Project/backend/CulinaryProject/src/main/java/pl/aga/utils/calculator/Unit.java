package pl.aga.utils.calculator;

/**
 * Created by RENT on 2017-08-02.
 */
public enum Unit {
    SPOON, TEASPOON, GLASS, GRAM, MILLIGRAM, LITER, MILLILITER
}
