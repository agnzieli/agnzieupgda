package pl.aga.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@AssociationOverrides({
        @AssociationOverride(name = "pk.recipe",
                joinColumns = @JoinColumn(name = "recipe_id")),
        @AssociationOverride(name = "pk.product",
                joinColumns = @JoinColumn(name = "product_id")) })
public class Ingredient {
    @EmbeddedId
    @JsonIgnore
    private IngredientId pk = new IngredientId();
    private double amount;

    public Ingredient(IngredientId pk, double amount) {
        this.pk = pk;
        this.amount = amount;
    }

    public Ingredient() {
    }

    public IngredientId getPk() {
        return pk;
    }

    public void setPk(IngredientId pk) {
        this.pk = pk;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Recipe getRecipe(){
        return pk.getRecipe();
    }

    public void setRecipe(Recipe recipe){
        pk.setRecipe(recipe);
    }

    public Product getProduct() {
        return pk.getProduct();
    }

    public void setRecipe(Product product) {
        pk.setProduct(product);
    }
}
