package pl.aga.utils.randomRecipes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.aga.entity.Recipe;
import pl.aga.repository.RecipeRepository;

import java.util.*;

@CrossOrigin
@RestController
@RequestMapping("/randomRecipes")
public class RandomRecipe {


    @Autowired
    private RecipeRepository recipeRepository;

    @RequestMapping("/get")
    public List<Recipe> getRandomList() {
        List<Recipe> all = recipeRepository.findAll();
        List<Recipe> ret = new ArrayList<>();
        List<Integer> randoms = new ArrayList<>();


        if (all.size() >= 5) {
            int z = 0;
            for (int i = 0; i < 5; i++) {
                int random = new Random().nextInt((all.size() - 1) + 1);
                for (int j = 0; j < randoms.size(); j++) {
                    if (random == randoms.get(j))
                        z = 1;
                }
                if (z != 1) {
                    Recipe r = all.get(random);
                    ret.add(r);
                    randoms.add(random);
                } else {
                    i--;
                    z = 0;
                }
            }
            return ret;
        } else {
            return all;
        }
    }
}



