package pl.aga.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.aga.entity.Product;
import pl.aga.entity.Recipe;

import java.math.BigInteger;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long>, CrudRepository<Product, Long> {

    @Query(value="SELECT id FROM recipe WHERE id IN (SELECT recipe_id FROM INGREDIENT WHERE product_id IN (SELECT id FROM product WHERE id = ?1))", nativeQuery = true)
    List<BigInteger> findById(long id);

    @Query(value="SELECT id FROM product WHERE UPPER(name) = UPPER(?1)", nativeQuery = true)
    List<BigInteger> findByName(String name);

}
