package pl.aga.utils.browser;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.aga.entity.Recipe;

import pl.aga.repository.RecipeRepository;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/browse")
public class Browser {

    @Autowired
    private RecipeRepository recipeRepository;

    @RequestMapping("/browser/product")
    public List<Recipe> showRecipesWithProduct(@RequestParam(name = "prodName") String prodName){
        List<BigInteger> list = recipeRepository.findByProductNameContaining(prodName);
        return getRecipes(list);
    }

    private List<Recipe> getRecipes(List<BigInteger> list) {
        List<Long> myList = new LinkedList<>();
        for(BigInteger bi : list){
            Long l = bi.longValue();
            myList.add(l);
        }
        List<Recipe> recipes = new LinkedList<>();
        for(Long l : myList){
            Recipe r = recipeRepository.findOne(l);
            recipes.add(r);
        }
        return recipes;
    }

    @RequestMapping("/browser/recipe")
    public List<Recipe> showRecipesWithName(@RequestParam(name = "recipeName") String recipeName){
        List<BigInteger> list = recipeRepository.findByNameContaining(recipeName);
        return getRecipes(list);
    }
}
