var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8088/';
app.config(function($routeProvider) {
    var path = '/views/';
    $routeProvider
    .when('/', {
        templateUrl: path + 'main.html',
        controller: 'myMainController'
    })
    .when('/productsAll', {
        templateUrl: path + 'productsShow.html',
        controller: 'productsShowController'
    })
    .when('/recipesAll', {
        templateUrl: path + 'recipesShow.html',
        controller: 'recipesShowController'
    })
    .when('/productAddNew', {
        templateUrl: path + 'productAdd.html',
        controller: 'addNewProductController'
    })
    .when('/recipeAddNew', {
        templateUrl: path + 'recipeAdd.html',
        controller: 'addNewRecipeController'
    })
    .when('/productShow/:id', {
        templateUrl: path + 'productShowOne.html',
        controller: 'showOneProductController'
    })
    .when('/productEdit/:id',{
         templateUrl: path + 'productEdit.html',
         controller: 'editProductController'
    })
    .when('/recipeShow/:id', {
        templateUrl: path + 'recipeShowOne.html',
        controller: 'showOneRecipeController'
    })
    .when('/recipeEdit/:id', {
        templateUrl: path + 'recipeEdit.html',
        controller: 'editRecipeController'
    })
    .when('/productDelete/:id', {
        templateUrl: path + 'productDelete.html',
        controller: 'deleteProductController'
    })
    .when('/recipeDelete/:id', {
        templateUrl: path + 'recipeDelete.html',
        controller: 'deleteRecipeController'
    })
    .when('/calculator', {
        templateUrl: path + 'calculator.html',
        controller: 'calculatorController'
    })
    .when('/productRecipes/:id', {
        templateUrl: path + 'recipesWithProduct.html',
        controller: 'recipesWithProductController'
    })
    .when('/browse', {
        templateUrl: path + 'browse.html',
        controller: 'browserController'
    })
    .when('/displayResults', {
        templateUrl: path + 'displayResults.html',
        controller: 'displayResultsController'
    })
    .when('/redir', {
        templateUrl: path + 'redir.html'
    })

});

app.controller('myMainController', function($scope, $http) {
    $http({
        url: url + 'randomRecipes/get',
        dataType: 'json'
    }).then(function(success){
        if(success.data){
        $scope.randomRecipes = success.data;
        $scope.message = 'Miłego dnia! :)'
        } else 
            $scope.message = 'Za mało przepisów w bazie...';
    }), function(error){
        console.error(error);
}
})

app.service('searchResults', function() {
    this.results = undefined;
    this.override = function(res) {
        this.results = res;
    }
    this.get = function() {
        return this.results;
    }
});

app.controller('productsShowController', function($scope, $http) {
    $http({
        url: url + 'products/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.products = success.data;
    }, function(error){
        console.error(error);
    });
});
    
app.controller('recipesShowController', function($scope, $http) {
     $http({
        url: url + 'recipes/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.recipes = success.data;
    }, function(error){
        console.error(error);    
     })
})

app.controller('addNewProductController', function($scope, $http) {
    $scope.products = [];
    $scope.addProduct = function() {
        $http({
            url: url + 'products/add',
            dataType: 'json',
            params: {
                name: $scope.productName,
                kcal: $scope.productKcal,
                carbo: $scope.productCarbo,
                fat: $scope.productFat,
                protein: $scope.productProtein,
                description: $scope.productDesc
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.products.push(success.data);
                alert ("Dodano produkt!");
            } else
                alert ("Taki produkt prawdopodobnie istnieje już w bazie... (lub wystąpił inny błąd)");
            }, function(error) {
            alert ('Wypełnij proszę wszystkie pola');
            console.error(error);
        });
    }
});

app.controller('editProductController', function($scope, $http, $routeParams) {

    $http({
        url: url + 'products/show/' + $routeParams.id,
        dataType: 'json'
    }).then(function(succ) {
        $scope.product = succ.data;
    }, function(err) {
        console.error(err);
    });
    
    $scope.editProduct = function() {
        $http({
            url: url + 'products/edit/' + $routeParams.id,
            dataType: 'json',
            params: {
                name: $scope.product.name,
                kcal: $scope.product.kcal,
                carbo: $scope.product.carbo,
                fat: $scope.product.fat,
                protein: $scope.product.protein,
                description: $scope.product.description          
            }
        }).then(function(succ) {
            $scope.product = succ.data;
            alert ('Edycja wykonana poprawnie!');
        }, function(err) {
            console.error(err);
        })
    }
});

app.controller('showOneProductController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'products/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.product = success.data;
    }, function(error) {
        console.error(error);
    });
});

app.controller('deleteProductController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'products/delete/' + id,
        dataType: 'json'
    }).then(function(succ) {
        $scope.message = "Produkt został usunięty poprawnie!";
        alert ("Usunięto produkt");
    }, function(err) {
        $scope.message = "Coś poszło nie tak...";
        console.error(err);
    });
});

app.controller('addNewRecipeController', function($scope, $http, $routeParams) {
    $scope.recipe = [];
    $scope.ingredients = [];
    
     $http({
        url: url + 'products/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.products = success.data;
    }, function(error){
        console.error(error);
    });
    
    
    $scope.addRecipeName = function(recipe) {
        $http({
            url: url + 'recipes/add',
            dataType: 'json',
            params: {
                name: $scope.recipeName
            }
        }).then(function(success) {
            console.log((success.data));
            if(success.data.id > 0) {
                $scope.recipe.push(success.data);
                $scope.recipeId = success.data.id;
                alert ("Dodano nazwę przepisu!");
                var form2 = $('#form2');
                form2.css('display', 'block');
                }  else
                alert ("Wystąpił błąd...");
            }, function(error) {
            console.error(error);
        });
    };
    
    $scope.addIngredient = function() {
        var productId = $scope.productId;
        var amount = $scope.recipeAmount;
        $http({
            url: url + 'recipes/add/recipe/' + $scope.recipeId + '/product/' + productId + '/amount/' + amount,
            dataType: 'json',
        }).then(function(success){
            $scope.ingredients.push(success.data);
            alert ("Dodano składnik");
            var form3 = $('#form3');
                form3.css('display', 'block');
        }, function(error) {
            alert ('Najpierw podaj nazwę przepisu!');
            console.error(error);
        });
    }
    $scope.addRecipe = function() {
        $http({
            url: url + 'recipes/add/' + $scope.recipeId,
            dataType: 'json',
            params: {
                description: $scope.recipeDesc,
                type: $scope.recipeType
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.recipe.push(success.data);
                console.log($scope.recipeType);
                alert ("Dodano przepis!");
            } else
                alert ("Wystąpił błąd, nie dodano przepisu...");
            }, function(error) {
            console.error(error);
        });
    }
});

app.controller('calculatorController', function($scope, $http){
    $scope.calculate = function() {
        $http({
            url: url + 'calculator/calculate',
            dataType: 'json',
            params: {
                product: $scope.productToCalc,
                from: $scope.fromToCalc,
                to: $scope.toToCalc,
                amount: $scope.amountToCalc
            }
        }).then(function(succ) {
            $scope.result = succ.data;
            alert ('WYNIK: ' + $scope.result );
        }, function(err) {
            alert ('Hola, hola! Tak nie przeliczamy! ;)');
            console.error(err);
        });
    }
});

app.controller('browserController', function($scope, $http, $window, searchResults){
    $scope.searchByProduct = function() {
        $http({
            url: url + 'browse/browser/product',
            dataType: 'json',
            params: {
                prodName: $scope.prodName
            }
        }).then(function(success) {
            $scope.rResult = success.data;
            if(success.data.length != 0){
            searchResults.override($scope.rResult);
            $window.location.href = '/#!displayResults';
            }else {
                alert ('Nic nie znaleziono... :(');
            }        
        }, function(error){
            console.error(error);
        });
    }
    
     $scope.searchByName = function() {
        $http({
            url: url + 'browse/browser/recipe',
            dataType: 'json',
            params: {
                recipeName: $scope.recipeName
            }
        }).then(function(success) {
            $scope.rResult = success.data;
            if(success.data.length != 0){
            searchResults.override($scope.rResult);
            $window.location.href = '/#!displayResults';
            } else {
                alert ('Nic nie znaleziono... :(');
            }
        }, function(error){
            console.error(error);
        });
    }
    
});

app.controller('displayResultsController', function($scope, searchResults) {
    $scope.rResult = searchResults.get();
});


app.controller('showOneRecipeController', function($scope, $http, $routeParams) {
        
    var id = $routeParams.id;
    $http({
        url: url + 'recipes/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.recipe = success.data;
        
        var recipe = $scope.recipe;
        var ingredients = recipe.ingredients;
        recipe.values = {
            kcal: 0,
            carbo: 0,
            fat: 0,
            protein: 0
        };
        for (var i of ingredients){
            var amount = parseInt(i.amount);
            recipe.values.kcal += Math.round(amount / 100 * parseInt(i.product.kcal));
            recipe.values.carbo += Math.round(amount / 100 * parseInt(i.product.carbo));
            recipe.values.fat += Math.round(amount / 100 * parseInt(i.product.fat));
            recipe.values.protein += Math.round(amount / 100 * parseInt(i.product.protein));
        }
        recipe['total'] = recipe.values;
        $scope.kcalSum = recipe['total'].kcal;
        $scope.carboSum = recipe['total'].carbo;
        $scope.proteinSum = recipe['total'].protein;
        $scope.fatSum = recipe['total'].fat;
        
    }, function(error) {
        console.error(error);
        });
    
});

app.controller('deleteRecipeController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'recipes/delete/' + id,
        dataType: 'json'
    }).then(function(succ) {
       
        alert ("Usunięto przepis");
    }, function(err) {
        console.error(err);
    });
});

app.controller('editRecipeController', function($scope, $http, $routeParams) {
    
     $http({
        url: url + 'products/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.products = success.data;
    }, function(error){
        console.error(error);
    });
    
    $scope.editRecipeName = function(recipe) {
        $http({
            url: url + 'recipes/edit/' + $routeParams.id,
            dataType: 'json',
            params: {
                name: $scope.recipeName
            }
        }).then(function(success) {
            alert ("Zmienionio nazwę!");
         }, function(error){
            console.error(error);
        });
    };
    
    $scope.editIngredient = function() {
        var productId = $scope.productId;
        var amount = $scope.recipeAmount;
        $http({
            url: url + 'recipes/edit/recipe/' + $routeParams.id + '/product/' + productId + '/amount/' + amount,
            dataType: 'json',
        }).then(function(success){
            alert ("Dodano inny składnik!");
        }, function(error){
            console.error(error);
        });
    }
    
    $scope.editRecipeDetails = function() {
        $http({
            url: url + '/recipes/edit/details/' + $scope.recipeId,
            dataType: 'json',
            params: {
                description: $scope.recipeDesc,
                type: $scope.recipeType
            }
        }).then(function(success){
            if(success.data.id > 0){
                $scope.message = "Zmieniono przepis!";
            } else
                alert ("Wystąpił błąd, nie wyedytowano przepisu...");
        }, function(error){
            console.error(error);
        })
    }
})

app.controller('recipesWithProductController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'products/recipes/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.productRecipes = success.data;
    }, function(error){
        console.error(error);
    });
});

