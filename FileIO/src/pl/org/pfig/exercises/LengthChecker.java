package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {

	private final String path = "resources/";

	private boolean isProperLength(String arg, int len) {

		if (arg.length() > len) {
			return true;
		}
		return false;

	}

	private String[] readFile(String filename) {

		String[] ret = new String[countLines(filename)];
		File f = new File(path + filename);

		try (Scanner sc = new Scanner(f)){ //taki skaner zamknie sie automatycznie
			int j = 0;
			while (sc.hasNextLine()) {
				
				ret[j++] = sc.nextLine();
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		return ret;

	}

	private int countLines(String filename) {
		File f = new File(path + filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return lines;

	}

	private void writeFile(String[] fileContent, int len) {
		
		try {
			FileOutputStream fosF = new FileOutputStream(path + "words_" + len + ".txt");

			PrintWriter pwF = new PrintWriter(fosF);

			for (String line : fileContent) {
				if(isProperLength(line, len))
				pwF.println(line);
			}
			pwF.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	public void make(String fileInput, int len) {
		String[] fileContent = readFile(fileInput);
		writeFile(fileContent, len);
			
		}

	}


