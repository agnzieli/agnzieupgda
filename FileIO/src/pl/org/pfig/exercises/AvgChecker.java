package pl.org.pfig.exercises;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class AvgChecker {

	private String filename;
	private final String path = "resources/";

	public AvgChecker(String filename) {
		this.filename = filename;
	}

	public void process() {

		File f = new File(path + filename);
		double avg = 0;
		LinkedList<String> fileContent = new LinkedList<>();

		try {

			Scanner sc = new Scanner(f);
			String currentLine;
			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();

				if (!currentLine.equals("")) {
					fileContent.add(currentLine);
					avg += countAvgFromLines(currentLine);
				}
			}
			avg /= fileContent.size();

			sc.close();
			
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			
		
			for (String line : fileContent) {
				if (countAvgFromLines(line) > avg) {
					pw.println(line);
				}
			}
			pw.close();

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	private double countAvgFromLines(String line) {
		String[] marks = line.split("\t");
		double marksSum = 0;
		for (int i = 1; i < marks.length; i++) {
			marksSum += Double.parseDouble(marks[i]);
		}
		return marksSum / (marks.length - 1);
	}
}
