package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class Columner {

	private String filename;
	private double[] currentColumn;
	private final String path = "resources/";
	private LinkedList<String> fileContent = new LinkedList<>();

	public Columner(String filename) {
		this.filename = filename;
	}

	public Columner() {
	}

	public double sumColumn(int column) throws WrongColumnExeption {
		readColumn(column);
		double sum = 0;
		for (double d : currentColumn) {
			sum += d;
		}

		return sum;
	}

	public double avgColumn(int column) throws WrongColumnExeption {

		return sumColumn(column) / fileContent.size();
	}

	public int countColumn(int column) throws WrongColumnExeption {
		readColumn(column);
		return fileContent.size();
	}

	public double maxColumn(int column) throws WrongColumnExeption {

		double max = readColumn(column)[0];

		for (int i = 1; i < readColumn(column).length; i++) {
			if (readColumn(column)[i] > max) {
				max = readColumn(column)[i];
			}
		}
		return max;

	}

	public double minColumn(int column) throws WrongColumnExeption {

		double min = readColumn(column)[0];

		for (int i = 0; i < readColumn(column).length; i++) {
			if (readColumn(column)[i] < min) {
				min = readColumn(column)[i];
			}
		}

		return min;
	}

	private void readFile() {
		if (fileContent.size() == 0) {
			File f = new File(path + filename);
			try {
				Scanner sc = new Scanner(f);
				while (sc.hasNextLine()) {
					fileContent.add(sc.nextLine());
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
		}

	}

	public void writeColumn(String filename, int column) throws WrongColumnExeption {
		
		try {
			FileOutputStream fos = new FileOutputStream(path + filename);
			PrintWriter pw = new PrintWriter(fos);
			for (double l : readColumn(column)){
				pw.println(l);
			}
			pw.close();
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	public double[] readColumn(int column) throws WrongColumnExeption {
		readFile();
		int maxColNum = fileContent.get(0).split("\t").length;
		if (column >= 1 && column <= maxColNum) {
			currentColumn = new double[fileContent.size()];

			int i = 0;
			for (String line : fileContent) {
				String[] cols = line.split("\t");
				currentColumn[i++] = Double.parseDouble(cols[column - 1].replace(",", "."));

			}
		} else {
			throw new WrongColumnExeption("Bad column value");
		}
		return currentColumn;

	}

}
