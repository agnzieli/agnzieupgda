package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class Sentencer extends Sentence {
	
	public void writeSentence(String filename, String sentence){
		File f = new File("resources/" + filename);
		
		try {
			FileOutputStream fos = new FileOutputStream(f);
			// domyslnie FOS nadpisuje ca�y plik
			//jezeli chcemy dopisywac do pliku (tryb append), to musimy przekazac drugi parametr do konstruktora FOS, np ....(f, true)
			
			PrintWriter pw = new PrintWriter(fos);
			
			pw.println(sentence);
			pw.close();
			
		} catch (FileNotFoundException e) {
			
			System.out.println("File not found");
		}
	}

}
