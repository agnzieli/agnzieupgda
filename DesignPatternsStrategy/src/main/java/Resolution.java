/**
 * Created by RENT on 2017-06-29.
 */
public enum Resolution {
            r1920x1080,
            r800x600,
            r1024x768,
            r640x480
}
