import com.sun.org.apache.bcel.internal.classfile.Code;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by RENT on 2017-06-29.
 */
public class ScreenRecorder {

    private Profile profile;
    private boolean rec = false;

    List<Profile> profileList = new ArrayList<Profile>();

    public void addProfile(Profile newProfile){

       profileList.add(newProfile);
    }

    public void setProfile(int profileIndex){
        if (!(profileIndex >= 0 && profileIndex <= profileList.size())) {
            throw new IllegalArgumentException();
        } else {
            profile = profileList.get(profileIndex);
        }
    }
    public void listProfiles(){
        for (Profile pr : profileList){
            System.out.println(profileList.indexOf(pr) + ". \tCodec: " + pr.getCodec() + ", resolution: " + pr.getResolution() + ", extension: " + pr.getExtension());
        }
    }

    public void startRecording(){
        if (profile == null){
            System.out.println("No profile selected");
        }
        else {
            System.out.println("Recording in progress");
            rec = true;
        }

    }

    public void stopRecording(){
        if(rec = true){
            System.out.println("Recoring stopped");
            rec = false;
        } else {
            System.out.println("No recording, there is nothing to be stopped");
        }

    }

}
