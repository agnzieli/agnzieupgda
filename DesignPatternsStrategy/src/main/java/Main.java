import java.util.Scanner;

/**
 * Created by RENT on 2017-06-29.
 */
public class Main {


    public static Profile createProfile() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Codec: "); // h264,h263,theora,vr8
        String codecType = sc.nextLine();
        Codec codec = Codec.valueOf(codecType);

        System.out.println("Extension: "); //
        String extensionType = sc.nextLine();
        Extension extension = Extension.valueOf(extensionType);

        System.out.println("Resolution: "); //r1920x1080,r800x600,r1024x768,r640x480
        String resolutionType = sc.nextLine();
        Resolution resolution = Resolution.valueOf(resolutionType);

        return new Profile(codec, extension, resolution);
    }

    public static void main(String[] args) {

        ScreenRecorder sr = new ScreenRecorder();
        Scanner sc = new Scanner(System.in);

        System.out.println("Choose number:");
        System.out.println("1. Add new profile");
        System.out.println("2. List existing profiles");
        System.out.println("3. Select profile");
        System.out.println("4. Start recording");
        System.out.println("5. Stop recording");
        System.out.println("6. EXIT");

        boolean exit = false;
        while (!exit) {
            System.out.println("Choose number from menu:");
        int choice = sc.nextInt();

            switch (choice) {
                case 1:
                    sr.addProfile(createProfile());
                    break;
                case 2:
                    sr.listProfiles();
                    break;
                case 3:
                    System.out.println("Give me index of profile you want to choose");
                    int a = sc.nextInt();
                    sr.setProfile(a);
                    break;
                case 4:
                    sr.startRecording();
                    break;
                case 5:
                    sr.stopRecording();
                    break;
                case 6:
                    exit = true;
                    break;
            }
        }
    }


}

