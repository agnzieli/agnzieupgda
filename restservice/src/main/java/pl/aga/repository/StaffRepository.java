package pl.aga.repository;

import org.springframework.data.repository.CrudRepository;
import pl.aga.entity.Staff;

/**
 * Created by RENT on 2017-07-27.
 */
public interface StaffRepository extends CrudRepository<Staff, Long> {
}
