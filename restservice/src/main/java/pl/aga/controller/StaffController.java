package pl.aga.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.aga.entity.Profession;
import pl.aga.entity.Staff;
import pl.aga.repository.ProfessionRepository;
import pl.aga.repository.StaffRepository;

import java.util.List;

/**
 * Created by RENT on 2017-07-27.
 */
@CrossOrigin
@RestController
@RequestMapping("/staff")
public class StaffController {

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private ProfessionRepository professionRepository;

    @RequestMapping({"/"})
    public String staff(){
        return "";
    }

    @RequestMapping("/show")
    public List<Staff> listStaff(){
        return (List<Staff>)staffRepository.findAll();
    }

    @RequestMapping("/add")
    public Staff addStaff(@RequestParam(name = "name") String name,
                          @RequestParam(name = "lastname") String lastname,
                          @RequestParam(name = "salary") String salary,
                          @RequestParam(name = "profession")String prof){
        long professionId = Long.valueOf(prof);
        double prSalary = Double.valueOf(salary);
        Profession p = professionRepository.findOne(professionId);
        Staff s = new Staff();
        s.setName(name);
        s.setLastname(lastname);
        s.setSalary(prSalary);
        s.setProfession(p);

        return staffRepository.save(s);
    }

    @RequestMapping("/show/{id}")
    public Staff showStaffById(@PathVariable("id") String id){
        long staffId = Long.valueOf(id);
        return staffRepository.findOne(staffId);
    }
}
