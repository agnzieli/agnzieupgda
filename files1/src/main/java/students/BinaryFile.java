package students;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BinaryFile implements IFile {
	
	private final static String FILE_NAME = "binary.txt";

	@Override
	public void save(List<Student> studentsList) {
		DataOutputStream out = null;
		
		try {
			FileOutputStream fout = new FileOutputStream(FILE_NAME);
			BufferedOutputStream bouf = new BufferedOutputStream(fout);
			out = new DataOutputStream(bouf);
			
			for(Student student : studentsList){
				out.writeInt(student.getIndexNumber());
				out.writeUTF(student.getName());
				out.writeUTF(student.getSurname());
				
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (out != null){
				try{
					out.close();
				} catch (IOException e){
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public List<Student> load() {
		
		List<Student> students = new ArrayList<>();
		
		try (DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(FILE_NAME)))){
			
			while(in.available() > 0){ //dopoki cokolwiek jest dostepne w tym pliku
				int indexNumber = in.readInt();
				String name = in.readUTF();
				String surname = in.readUTF();
				
				Student student = new Student (indexNumber, name, surname);
				students.add(student);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return students;
	}

}
