package students;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonFile implements IFile {
	
	private ObjectMapper mapper = new ObjectMapper();
	private final static String FILE_NAME = "studenci.json";

	@Override
	public void save(List<Student> studentsList) {
		
		try {
			mapper.writeValue(new File(FILE_NAME), studentsList);
		} catch (JsonGenerationException e) {			
			e.printStackTrace();
		} catch (JsonMappingException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
	
	}

	@Override
	public List<Student> load() {
		
		try {
			List<Student> students = mapper.readValue(new File(FILE_NAME), new TypeReference<List<Student>>() {});
			return students;
		} catch (JsonParseException e) {
			
			e.printStackTrace();
		} catch (JsonMappingException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	
		return null;
	}

}
