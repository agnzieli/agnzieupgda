package oop.teddybear;

public class TeddyBearMain {

	public static void main(String[] args) {
		
		TeddyBear uszatek = new TeddyBear("Uszatek", 2, true);
		uszatek.PrintName();
		uszatek.ShowAge();
		uszatek.IsBrown();
		
		TeddyBear koralgol = new TeddyBear("Koralgol", 1, false);
		koralgol.PrintName();
		koralgol.ShowAge();
		koralgol.IsBrown();
		
		koralgol.setImie("Misiek");
		koralgol.PrintName();

	}

}
