package oop.teddybear;

public class TeddyBear {
	
	private String imie;
	private int wiek;
	private boolean brown;
	
	public TeddyBear(String imie, int wiek, boolean brown){
		this.imie = imie;
		this.wiek = wiek;
		this.brown = brown;
	}
	
	public void PrintName (){
		System.out.println(imie);
	}
	
	public void ShowAge(){
		System.out.println(wiek);
	}
	
	public void IsBrown(){
		System.out.println(brown);
	}
	
	public void setImie(String imie) {
		this.imie = imie;
	}
	
	public String getImie() {
		return imie;
	}
	
	public void setWiek(int wiek) {
		this.wiek = wiek;
	}
	
	public int getWiek() {
		return wiek;
	}
	
	public void setBrown(boolean brown) {
		this.brown = brown;
	}
	
	public boolean getBrown() {
		return brown;
	}
	

}
