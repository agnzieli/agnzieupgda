package oop.quadraticequasion;

public class QuadraticEquationMain {

	public static void main(String[] args) {
		
		QuadraticEquation rownanie1 = new QuadraticEquation(1,4,3);
		QuadraticEquation rownanie2 = new QuadraticEquation(1, -2, 1);
		
		rownanie1.Show();
		rownanie2.Show();

	}

}
