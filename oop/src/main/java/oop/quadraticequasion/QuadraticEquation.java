package oop.quadraticequasion;

public class QuadraticEquation {
	
	private double a;
	private double b;
	private double c;
	
	public QuadraticEquation(double a, double b, double c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public double calcDelta(){
		double delta = b*b - 4*a*c;
		return delta;
	}
	
	
	public double calcX1(){
		double X1=(-b-Math.sqrt(calcDelta()))/(2*a);
		return X1;
	}
	
	public double calcX2(){
		double X2=(-b+Math.sqrt(calcDelta()))/(2*a);
		return X2;
	}
	
	public void Show(){
		System.out.println("Delta =" + calcDelta() + ", x1 = " + calcX1() + ", x2 = " + calcX2());
	}


	

}
