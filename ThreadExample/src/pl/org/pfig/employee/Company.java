package pl.org.pfig.employee;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-06-19.
 */
public class Company {

    private List<Employee> employees = new LinkedList<>();

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public void filter(FilterInterface f, TransformInterface t){
        for(Employee e : employees){
            if(f.test(e.getName())){
                System.out.println(t.transform(e.getName()));
            }
        }

    }
}
