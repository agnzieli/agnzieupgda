package pl.org.pfig.employee;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-06-19.
 */
public class Main {

    public static void main(String[] args) {

        List<Employee> emp = new LinkedList<>();
        emp.add(new Employee("Paweł", "Testowy", 18, 3000));
        emp.add(new Employee("Piotr", "Przykladowy", 32, 10000));
        emp.add(new Employee("Julia", "Doe", 41, 4300));
        emp.add(new Employee("Przemysław", "Wietrak", 56, 4500));
        emp.add(new Employee("Zofia", "Zaspa", 37, 3700));

        Company c = new Company();
        c.setEmployees(emp);


        c.filter((n) -> n.startsWith("P"), t -> t.toUpperCase());

    }
}
