package pl.org.pfig.calc;

/**
 * Created by RENT on 2017-06-19.
 */

@FunctionalInterface
public interface CalculatorInterface {
    public double doOperation(int a, int b);
}
