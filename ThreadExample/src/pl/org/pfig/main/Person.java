package pl.org.pfig.main;

/**
 * Created by RENT on 2017-06-19.
 */
public class Person {

    private int age;

    public Person(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
