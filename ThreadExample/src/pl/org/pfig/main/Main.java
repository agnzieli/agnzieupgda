package pl.org.pfig.main;

import pl.org.pfig.animals.Animal;
import pl.org.pfig.animals.AnimalInterface;
import pl.org.pfig.calc.Calc;
import pl.org.pfig.calc.CalculatorInterface;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-06-19.
 */
public class Main {
    public static void main(String[] args) {
        Thread t = new Thread(new PrzykladowyWatek());

        t.start();

        new Thread(new InnyWatek(), "Jakis watek").start();

        System.out.println("KONIEC");

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Pochodzę z kodu Runnable");

            }
        }).start();

        new Thread( () -> {
            int a = 1; int b = 3;
            System.out.println("Suma wynosi: " + (a+b));
        }).start();


        new Thread( () -> System.out.println("Wiadomosc z FI")).start();


        List<Person> p = new LinkedList<>();
        p.add(new Person(1));

        Calc c = new Calc();

       // System.out.println(c.make(3,4));

        System.out.println(c.oper(5, 6, new CalculatorInterface() {
            @Override
            public double doOperation(int a, int b) {
                return a+b;
            }
        }));

        System.out.println(c.oper(17,3, (a, b) -> (a-b)));
        System.out.println(c.oper(17,3, (a, b) -> (a*b)));

        Animal a = new Animal();
       /* a.addAnimal("słoń");

        System.out.println(a.containsAnimal("słoń"));
*/

       a.addAnimal("slon", (anims) -> (anims.split(" ")));
       a.addAnimal("pies|kot|zaba", (anims) -> (anims.split("\\|")));
       a.addAnimal("aligator_jaszczurka", (anims) -> (anims.split("_")));
       a.addAnimal("katp", (anims) -> {
           String[] ret = new String[1];
           ret[0] = "";
           for(int i = anims.length() - 1; i >= 0; i--) {
               ret[0] += anims.charAt(i) + "";
           }
           return ret;
       });

       /* System.out.println(a.containsAnimal("katp"));
        System.out.println(a.containsAnimal("ptak"));
*/

        System.out.println(a.containsAnimal("ptak|kot", (s) -> s.split("\\|") ));











    }
}
