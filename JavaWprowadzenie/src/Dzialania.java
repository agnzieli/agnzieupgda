
public class Dzialania {

	public static void main(String[] args) {
		
		System.out.print("2+3:");
		int a;
		a=2+3;
		System.out.println(a);
		
		System.out.print("2-4:");
		System.out.println(2-4);
		
		System.out.print("5/2: ");
		System.out.println(5/2);
		
		System.out.print("5.0/2: ");
		double b;
		b = 5.0/2;
		System.out.println(b);
		
		System.out.print("5/2.0: ");
		System.out.println(5/2.0);
		
		System.out.print("5.0/2.0: ");
		System.out.println(5.0/2.0);
		
		System.out.print("100L-10: ");
		long c;
		c = 100L-10;
		System.out.println(c);
		
		System.out.print("2f-3: ");
		System.out.println(2f-3);
		
		System.out.print("5f/2: ");
		System.out.println(5f/2);
		
		System.out.print("5d/2: ");
		System.out.println(5d/2);
		
		System.out.print("'A' + 2: ");
		int d;
		d = 'A' + 2;
		System.out.println(d);
				
		System.out.print("'a' + 2: ");
		int e;
		e = 'a'+2;
		System.out.println(e);
		
		System.out.print("\"a\" + 2: ");
		String f;
		f = "a"+2;
		System.out.println(f);
		
		System.out.print("\"a\" + \"b\": ");
		String g;
		g = "a"+"b";
		System.out.println(g);
		
		System.out.print("'a' + 'b': ");
		int h;
		h = 'a' + 'b';
		System.out.println(h);
		
		System.out.print("\"a\" + 'b': ");
		String i;
		i = "a"+'b';
		System.out.println(i);
		
		System.out.print("a + 'b' + 3: ");
		String j;
		j = "a"+'b'+3;
		System.out.println(j);
		
		System.out.print("'b' + 3 + a : ");
		String k;
		k = 'b'+3+"a";
		System.out.println(k);
		
		System.out.print("9%4: ");
		System.out.println(9%4);
		
		
		


	}

}
