
public class TablicaImion {

	public static void main(String[] args) {
		
		String[] imiona = {"Ola", "Ada", "Kamil", "Mateusz", "Alek"};
		
		System.out.println("P�tla forEach");
		for (String imie : imiona){
			System.out.println(imie);
		}
		
		System.out.println("P�tla for:");
		for (int i=0; i<imiona.length; i++){
			System.out.println(imiona[i]);
		}
		
		System.out.println("Co drugie imi�:");
		
		int i=0;
		while (i<imiona.length){
			System.out.println(imiona[i]);
			i=i+2;
		}
		
		for(int j=0; i<imiona.length; j+=2){
			System.out.println(imiona[j]);
		}
		
		System.out.println("Imiona zaczynaj�ce si� na 'A': ");
		for (String imie : imiona){
			
			if(imie.charAt(0)=='A'){
				System.out.println(imie);
			}
			
		}

}
}
