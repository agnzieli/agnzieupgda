
public class GeoMathTest {
	
	public void squareAreaTest(){
		assert GeoMath.squareArea(2.0) == 4.0;
		assert GeoMath.squareArea(1.0) == 1.0;
		assert GeoMath.squareArea(0) == 0;
	}
	
	public void cubeAreaTest(){
		assert GeoMath.cubeArea(2.0) == 24.0;
		assert GeoMath.cubeArea(1.0) == 6.0;
		assert GeoMath.cubeArea(0) == 0;
	}
	
	public void cubeVolumeTest(){
		assert GeoMath.cubeVolume(2.0) == 8.0;
		assert GeoMath.cubeVolume(1.0) == 1.0;
		assert GeoMath.cubeVolume(0) == 0;
	}
	
	public void circleAreaTest(){
		assert GeoMath.circleArea(2.0) == 4.0;
		assert GeoMath.circleArea(2.0) < 4.0*3.15;
		assert GeoMath.circleArea(1.0) > 3.14;
		assert GeoMath.circleArea(1.0) < 3.14;
		assert GeoMath.circleArea(0) == 0;
	}
	
	public void cylinderVolume(){
		assert GeoMath.cylinderVolume(1.0, 1.0) > 3.14;
		assert GeoMath.cylinderVolume(1.0, 1.0) < 3.15;
		assert GeoMath.cylinderVolume(2.0, 1.0) > 6.28;
		assert GeoMath.cylinderVolume(2.0, 1.0) < 6.30;
		assert GeoMath.cylinderVolume(0, 0) == 0;
		
	}
	
	public void coneVolume(){
		assert GeoMath.coneVolume(1.0, 1.0) > 3.14 / 3;
		assert GeoMath.coneVolume(1.0, 1.0) < 3.15 / 3;
		assert GeoMath.coneVolume(2.0, 1.0) > 6.28 /3;
		assert GeoMath.coneVolume(2.0, 1.0) < 6.30 /3;
		assert GeoMath.coneVolume(0, 0) == 0;
		
	}
	
	public void pyramidVolume(){
		assert GeoMath.pyramidVolume(1.0, 1.0) == 1.0 / 3;
		assert GeoMath.pyramidVolume(2.0, 1.0) == 4.0 /3;
		assert GeoMath.pyramidVolume(0, 0) == 0;
		
	}
	
	

}
