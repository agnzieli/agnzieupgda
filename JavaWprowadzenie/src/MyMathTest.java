import org.junit.Test;

public class MyMathTest {
	
	@Test
	public void absIntegerTest(){
		assert MyMath.abs(4) == 4;
		assert MyMath.abs(-4) == 4;
	}
	
	@Test
	public void absDoubleTest(){
		assert MyMath.abs(4.5) == 4.5;
		assert MyMath.abs(-4.5) == 4.5;
	}
	
	@Test
	public void powIntegerTest(){
		assert MyMath.pow(2, 3) == 8;
		assert MyMath.pow(3, 2) == 9;
	}

	
	@Test
	public void powDoubleTest(){
		assert MyMath.pow(2.0, 3) == 8.0;
		assert MyMath.pow(3.0, 2) == 9.0;
	}
	
	@Test
	public void powFloatTest(){
		assert MyMath.pow(2f, 3) == 8f;
		assert MyMath.pow(3f, 2) == 9f;
	}
}
