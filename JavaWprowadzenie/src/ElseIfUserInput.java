import java.util.Scanner;

public class ElseIfUserInput {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("podaj liczb�");
		int x = scanner.nextInt();

		if (x % 3 == 0) {
			System.out.println("jest podzielna przez 3");
		} else {
			if (x % 2 == 0) {
				System.out.println("nie jest podzielna przez 3, ale jest parzysta");
			}
		}

		scanner.close();

	}

}
