package pl.org.pfig.data;

public class Dog implements AnimalInterface {
	
	@Override
	public String toString() {
		return "Dog: " + this.name;
	}

	private String name;

	public Dog(String name) {
		super();
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	
}
