
	import org.junit.Test;

	public class MetodyMatematykaTesty {
		
		
		@Test
		public void testIloczynu(){
			
			assert MetodyMatematyka.iloczyn(1, 1) == 1;
			assert MetodyMatematyka.iloczyn(0, 10) == 0;
			assert MetodyMatematyka.iloczyn(1, 10) == 10;
			
			
		}
		
		@Test 
		public void testMniejszaZDwoch(){
			assert MetodyMatematyka.mniejsza(3, 5) == 3;
			assert MetodyMatematyka.mniejsza(9, 5) == 5;
			assert MetodyMatematyka.mniejsza(1, 5) == 1;
			assert MetodyMatematyka.mniejsza(1, 1) == 1;
		}
		
		@Test 
		public void testMniejszaZTrzech(){
			assert MetodyMatematyka.mniejsza(3, 5, 7) == 3;
			assert MetodyMatematyka.mniejsza(9, 5, 0) == 0;
			assert MetodyMatematyka.mniejsza(1, 5, 7) == 1;
			assert MetodyMatematyka.mniejsza(5, 5, 5) == 5;
		}
		
		@Test 
		public void testSzukaj(){
			
			int[] liczby = {1,2,3,5,3};
			assert MetodyMatematyka.szukaj(liczby, 3) == 2;
			assert MetodyMatematyka.szukaj(liczby, 4) == -1;
			assert MetodyMatematyka.szukaj(liczby, 5) == 3;
			
		}
		
	

	}

