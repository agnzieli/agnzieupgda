
public class GeoMath {

	public static void main(String[] args) {
		

	}
	
	public static double squareArea (double a){
		return a*a;
	}
	
	public static double cubeArea (double a) {
		return 6*squareArea(a);
	}
	
	public static double circleArea (double r){
		return (Math.PI)*r*r;
	}
	
	public static double cylinderVolume (double r, double h){
		return circleArea(r)*h;
	}
	
	public static double coneVolume (double r, double h){
		return (1/3)*cylinderVolume(r, h);
	}
	
	public static double cubeVolume (double a){
		return squareArea(a)*a;
	}
	
	public static double pyramidVolume (double a, double h){
		return squareArea(a)*h/3;
	}

}
