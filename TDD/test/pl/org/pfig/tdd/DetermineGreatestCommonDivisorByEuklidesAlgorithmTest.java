package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineGreatestCommonDivisorByEuklidesAlgorithmTest {
	
	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}
	

	@Test
	public void whenDivisorIsZeroExceptionIsExpected() {
		int a = 5;
		int b = 0;
		IllegalArgumentException iae = null;
		try{
			e.determineGreatestCommonDivisorByEuklidesAlgorithm(a, b);
		} catch (IllegalArgumentException e){
			iae = e;
		}
		
		assertNotNull("Nie dzielimy przez 0!", iae);
	}
	
	@Test
	public void whenTwoProperValuesGivenProperResultExpected(){
		int[][] data = { { 48, 12, 12 }, { 50, -20, 10 }, { 1, 1, 1}, {12, 48, 12 }, { -9, -3, 3 }, { -50, 20, 10} };
		for (int i = 0; i < data.length; i ++){
		assertEquals(data[i][2], e.determineGreatestCommonDivisorByEuklidesAlgorithm(data[i][0], data[i][1]));				
			}
		

	}
	
	

}
