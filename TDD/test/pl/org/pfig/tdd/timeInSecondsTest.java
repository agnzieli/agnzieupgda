package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class timeInSecondsTest {
	
	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenHourIsNegativeExceptionIsExpected(){
		int[] data = { -3, 12, 15 };
		e.timeInSeconds(data[0], data[1], data[2]);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenMinuteIsNegativeExceptionIsExpected(){
		int[] data = { 3, -12, 15 };
		e.timeInSeconds(data[0], data[1], data[2]);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenSecondIsNegativeExceptionIsExpected(){
		int[] data = { 3, 12, -15 };
		e.timeInSeconds(data[0], data[1], data[2]);
	}

	@Test
	public void whenProperValuesAreGivenProperResultIsExpected() {
		int[] data = { 3, 12, 15, 11535 };
		assertEquals(data[3], e.timeInSeconds(data[0], data[1], data[2]));		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenHourIsOutOfRangeExceptionIsExpected(){
		int[] data = { 30, 12, 15 };
		e.timeInSeconds(data[0], data[1], data[2]);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenMinuteIsOutOfRangeExceptionIsExpected(){
		int[] data = { 3, 80, 15 };
		e.timeInSeconds(data[0], data[1], data[2]);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenSecondIsOutOfRangeExceptionIsExpected(){
		int[] data = { 3, 80, 90 };
		e.timeInSeconds(data[0], data[1], data[2]);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenAllValuesAreNegativeExceptionIsExpected(){
		int[] data = { -3, -12, -15 };
		e.timeInSeconds(data[0], data[1], data[2]);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenAllValuesAreOutOfRangeExceptionIsExpected(){
		int[] data = { 30, 80, 111 };
		e.timeInSeconds(data[0], data[1], data[2]);
	}
	
	

}
