package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineNameOfMonthTest {
	
	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}
		
	@Test
	public void whenGivenNumberIsOutOfRangeExceptionIsExpected(){
		int arg = 20;
		IllegalArgumentException iae = null;
		try {
			e.determineDayOfWeek(arg);
		} catch(IllegalArgumentException e){
			iae = e;
		}
		
		assertNotNull("Wyjatek wystapil", iae);
	}
	

	@Test
	public void whenProperValueIsGivenProperMonthResultExpected(){
		int[] data = { 1, 12 };
		String[] expected = { "styczen", "grudzien" };
		
		for (int i = 0; i <data.length; i++){
			assertEquals(expected[i], e.determineNameOfMonth(data[i]));
		}
	}
	
	@Test
	public void whenProperMediumValueIsGivenProperResultExpected(){
		int data = 6;
		String expected = "czerwiec";
		assertEquals(expected, e.determineNameOfMonth(data));
	}

}
