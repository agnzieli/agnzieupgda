package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineDayOfWeekTest {
	
	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}

	@Test
	public void whenGivenNumberIsOutOfRangeExceptionIsExpected() throws IllegalArgumentException {
		int data = -14;
		IllegalArgumentException iae = null;
		try {
			e.determineDayOfWeek(data);
		} catch(IllegalArgumentException e){
			iae = e;
		}
		
		assertNotNull("Wyjatek nie wystapil", iae);
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void whenOutOfRangePositiveArgumentIsGivenExceptionExpected(){
		int data = 123;
		e.determineDayOfWeek(data);
	}
	
	@Test
	public void whenProperMediumValueIsGivenProperResultExpected(){
		int data = 4;
		String expected = "sroda";
		assertEquals(expected, e.determineDayOfWeek(data));
	}
	
//	@Test
//	public void whenNumberIsGivenProperNameOfDayIsExpected(){
//		
//		String[] expected = { "Niedziela", "Poniedzialek", "Wtorek", "Sroda", "Czwartek", "Piatek", "Sobota" };
//		for(int arg = 1; arg <= 7; arg++){
//		assertEquals(expected[arg-1], e.determineDayOfWeek(arg));
//		}
//	}
	
	@Test
	public void whenProperValueIsGivenProperDayResultExpected(){
		int[] data = { 1, 7 };
		String[] expected = { "niedziela", "sobota" };
		
		for (int i = 0; i <data.length; i++){
			assertEquals(expected[i], e.determineDayOfWeek(data[i]));
		}
	}

}
