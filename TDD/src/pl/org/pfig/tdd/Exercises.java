package pl.org.pfig.tdd;

import java.util.Scanner;

public class Exercises {
	public int absoluteValue(int num){
		if(num < 0){
			num = -num;
		}
		return num;
	}
	
	public String determineDayOfWeek(int day){
				
		if (day > 7 || day < 1){
			throw new IllegalArgumentException();
		}
		
		String[] days = { "niedziela", "poniedzialek", "wtorek", "sroda", "czwartek", "piatek", "sobota" };
		
		return days[day - 1];
			
	}
	
	public String determineNameOfMonth(int month){
		
		if (month > 12 || month < 1){
			throw new IllegalArgumentException();
		}
		
		String[] months = { "styczen", "luty", "marzec", "kwiecien", "maj", "czerwiec", "lipiec", "sierpien", "wrzesien", "pazdziernik", "listopad", "grudzien" };
		
		return months[month - 1];
	}
	
	public int determineGreatestCommonDivisorByEuklidesAlgorithm(int a, int b){
		
		if (b == 0 || a == 0){
			throw new IllegalArgumentException();
		}
		
		while (a % b != 0){
			int c = a % b; 
			a = b;
			b = c;
		}
		if (b < 0 ){
			return -b;
		}
		
		return b;
	}
	
	public int timeInSeconds (int hour, int minute, int second){
		if ( hour < 0 || hour > 24 || minute < 0 || minute > 60 || second < 0 || second > 60){
			throw new IllegalArgumentException();
		}
				
		return (hour * 3600 + minute * 60 + second);
	}
	
	public int[] evenAndOddNumbersFromRange (int a, int b){
		
		int[] res = new int[b - a + 1];
		for (int i = 0; i < res.length; i++){
			for (int j = a; j <= b; j++){
			if ( j % 2 == 0 ){
				res[i] = j;
			}
			if ( j % 2 != 0 ){
				
			}
		}
		}
		
		return null;
	}

}
