var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8088/';
app.config(function($routeProvider) {
    var path = './views/';
    $routeProvider
        .when('/', {
            templateUrl: path + 'main.html'
        })
        .when('/elephant', {
            templateUrl: path + 'elephant.html',
            controller: 'elephantController'
        })
        .when('/crocodile', {
            templateUrl: path + 'crocodile.html',
            controller: 'crocodileController'
        })
        .when('/animals', {
                templateUrl: path + 'animals.html',
                controller: 'animalsController'
        })
        .when('/mammals', {
                templateUrl: path + 'mammals.html',
                controller: 'mammalsController'
        })
        .when('/reptiles', {
                templateUrl: path + 'reptiles.html',
                controller: 'reptilesController'
        })
});

app.controller('elephantController', function($scope, $http) {
    $scope.message = "Jestem słoniem i jestem duży :)";
    $http({
        url: url + 'elephant',
        dataType: 'json',
        params: {}
    }).then(function(success) {
        $scope.name = success.data.name;
    }, function(error) {
        console.error(error);
    });
});
    
app.controller('crocodileController', function($scope, $http){
    $scope.message = "Jestem krokodylkiem i jestem zielony!";
    $http({
        url: url + 'crocodile',
        dataType: 'json',
        params: {}
    }).then(function(success){
        $scope.name = success.data.name;
    }, function(error) {
        console.error(error);
    });
});

app.controller('animalsController', function($scope, $http) {
    $http({
        url: url + 'animals',
        dataType: 'json',
        params: {}
    }).then(function(success) {
        var root = $('.animals');
        var animals = success.data;
        for(let i = 0; i < animals.length; i++) {
            $('<li/>').html(animals[i].name).appendTo(root);
        }
    }, function(error) {
        console.error(error);
    });
});

app.controller('mammalsController', function($scope, $http) {
    $http({
        url: url + 'mammals',
        dataType: 'json',
        params: {}
    }).then(function(success) {
        var root = $('.mammals');
        var mammals = success.data;
        for(let i = 0; i < mammals.length; i++) {
            $('<li/>').html(mammals[i].name).appendTo(root);
        }
    }, function(error) {
        console.error(error);
    });
});

app.controller('reptilesController', function($scope, $http) {
    $http({
        url: url + 'reptiles',
        dataType: 'json',
        params: {}
    }).then(function(success){
        var root = $('.reptiles');
        var keys = Object.keys(success.data);
        for (let i = 0; i < keys.length; i++) {
            $('<li/>').html(keys[i] + " " + success.data[keys[i]].name).appendTo(root);
        }
   
    }, function(error){
        console.error(error);
    });
});

